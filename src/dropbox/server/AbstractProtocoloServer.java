/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Arthur
 */
public abstract class AbstractProtocoloServer extends UnicastRemoteObject implements ProtocoloServer {

    private final ServerReference reference;

    public AbstractProtocoloServer(ServerReference reference) throws RemoteException {
        this.reference = reference;
    }

    @Override
    public final ServerReference getInfo(int serverId) throws RemoteException {
        return this.reference;
    }

    @Override
    public final void heartBeat(int serverId) throws RemoteException {
        this.recebeuHeartBeat(serverId);
    }

    protected abstract void recebeuHeartBeat(int serverId) throws RemoteException;
}
