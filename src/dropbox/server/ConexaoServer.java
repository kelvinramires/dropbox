/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.server;

import dropbox.util.HeartBeat;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.rmi.RemoteException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

/**
 *
 * @author Arthur
 */
public abstract class ConexaoServer {
    private static final Logger logger = Logger.getLogger(ConexaoServer.class.getName());
    public static final String PROP_LASTHEARTBEAT = "lastHeartBeat";

    private final ProtocoloServerWrapper protocolo;
    private final transient PropertyChangeSupport propertyChangeSupport;
    private long lastHeartBeat;
    private AtomicBoolean morto;
    private ServerReference reference;
    private final HeartBeat heartBeat;

    ConexaoServer(ScheduledExecutorService threadPool, ProtocoloServerWrapper protocolo) throws RemoteException {
        this(threadPool, protocolo.getServerReference(), protocolo);
    }

    ConexaoServer(ScheduledExecutorService threadPool, int id, String host, int port, ProtocoloServerWrapper protocolo) {
        this(threadPool, new ServerReference(host, port, id), protocolo);
    }

    ConexaoServer(ScheduledExecutorService threadPool, ServerReference reference, ProtocoloServerWrapper protocolo) {
        this.morto = new AtomicBoolean(false);
        this.heartBeat = new HeartBeatImpl(threadPool, 500, 1000);
        this.reference = reference;
        this.protocolo = protocolo;
        this.propertyChangeSupport = new PropertyChangeSupport(this);
    }

    public ServerReference getReference() {
        return reference;
    }
    
    /**
     * Get the value of lastHeartBeat
     *
     * @return the value of lastHeartBeat
     */
    public long getLastHeartBeat() {
        return lastHeartBeat;
    }

    /**
     * Set the value of lastHeartBeat
     *
     * @param lastHeartBeat new value of lastHeartBeat
     */
    private void setLastHeartBeat(long lastHeartBeat) {
        long oldLastHeartBeat = this.lastHeartBeat;
        this.lastHeartBeat = lastHeartBeat;
        this.propertyChangeSupport.firePropertyChange(PROP_LASTHEARTBEAT, oldLastHeartBeat, lastHeartBeat);
    }
    
    /**
     * Add PropertyChangeListener.
     *
     * @param listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Add PropertyChangeListener.
     *
     * @param propertyName
     * @param listener
     */
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * Remove PropertyChangeListener.
     *
     * @param listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
    /**
     * Remove PropertyChangeListener.
     *
     * @param propertyName
     * @param listener
     */
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
    }

    protected void recebeuHeartBeat() {
        this.heartBeat.hearbeatCallback(true);
    }

    ProtocoloServerWrapper getProtocolo() {
        return protocolo;
    }
    
    void scheduleHeartBeat() {
        this.heartBeat.schedule();
    }

    public ServerReference getInfo() {
        return this.reference;
    }

    private void morreuPrivate() {
        if (this.morto.getAndSet(true)) {
            return;
        }
        morreu();
    }
    
    protected abstract void morreu();

    private class HeartBeatImpl extends HeartBeat {
        public HeartBeatImpl(ScheduledExecutorService threadPool, long sendDelay, long waitDelay) {
            super(threadPool, sendDelay, waitDelay);
        }

        @Override
        protected void enviarHeartBeat() throws Exception {
            ConexaoServer.this.protocolo.heartBeat();
        }

        @Override
        public void hearbeatCallback(boolean sucesso) {
            super.hearbeatCallback(sucesso);
            if (sucesso) {
                ConexaoServer.this.setLastHeartBeat(System.currentTimeMillis());
            } else {
                ConexaoServer.this.morreuPrivate();
            }
        }
    }
}
