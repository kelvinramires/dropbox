/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.server;

import dropbox.db.model.Arquivo;
import java.rmi.RemoteException;

/**
 *
 * @author Arthur
 */
public class ProtocoloServerWrapper {
    private final ProtocoloServer wrapped;
    private final int localServerId;

    public ProtocoloServerWrapper(ProtocoloServer protocolo, int localServerId) {
        this.wrapped = protocolo;
        this.localServerId = localServerId;
    }
    
    public void conectei(String host, int porta) throws RemoteException {
        wrapped.conectei(new ServerReference(host, porta, localServerId));
    }

    public void adicionarUsuario(String login, String senha) throws RemoteException {
        wrapped.adicionarUsuario(localServerId, login, senha);
    }

    public void adicionarArquivo(Arquivo arquivo) throws RemoteException {
        wrapped.adicionarArquivo(localServerId, arquivo);
    }

    public void adicionarArquivo(Arquivo arquivo, byte[] dados) throws RemoteException {
        wrapped.adicionarArquivo(localServerId, arquivo, dados);
    }

    public byte[] lerArquivo(Arquivo arquivo) throws RemoteException {
        return wrapped.lerArquivo(localServerId, arquivo);
    }

    public void removerArquivo(Arquivo arquivo) throws RemoteException {
        wrapped.removerArquivo(localServerId, arquivo);
    }

    public void associarArquivo(Arquivo arquivo) throws RemoteException {
        wrapped.associarArquivo(localServerId, arquivo);
    }

    public void heartBeat() throws RemoteException {
        wrapped.heartBeat(localServerId);
    }

    public ServerReference getServerReference() throws RemoteException {
        return wrapped.getInfo(localServerId);
    }

    public void enviarArquivo(Arquivo arquivo, byte[] dados) throws RemoteException {
        wrapped.enviarArquivo(localServerId, arquivo, dados);
    }
}
