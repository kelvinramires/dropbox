/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.server;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.DisposableMap;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.FunctionList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import dropbox.util.CloseableIterator;
import dropbox.util.LockedIterator;
import java.util.ArrayList;

/**
 *
 * @author Arthur
 */
class ListaDeConexoes {
    private final BasicEventList<ConexaoServer> lista;
    private final DisposableMap<Integer, ConexaoServer> mapa;
    private EventList<ConexaoServer> listaSomenteLeitura;

    public ListaDeConexoes() {
        this.lista = new BasicEventList<>();

        this.mapa = GlazedLists.syncEventListToMap(lista, new FunctionList.Function<ConexaoServer, Integer>() {

            @Override
            public Integer evaluate(ConexaoServer sourceValue) {
                return sourceValue.getReference().getId();
            }
        });
    }

    public EventList<ConexaoServer> getReadOnlyView() {
        if (this.listaSomenteLeitura == null) {
            synchronized (this.lista) {
                if (this.listaSomenteLeitura == null) {
                    this.listaSomenteLeitura = GlazedLists.readOnlyList(lista);
                }
            }
        }
        return this.listaSomenteLeitura;
    }

    public void add(ConexaoServer conexao) {
        Lock writeLock = this.lista.getReadWriteLock().writeLock();
        writeLock.lock();
        try {
            this.lista.add(conexao);
        } finally {
            writeLock.unlock();
        }
    }

    public void remove(ConexaoServer conexao) {
        Lock writeLock = this.lista.getReadWriteLock().writeLock();
        writeLock.lock();
        try {
            this.lista.remove(conexao);
        } finally {
            writeLock.unlock();
        }
    }

    public ConexaoServer findById(int id) {
        Lock readLock = this.lista.getReadWriteLock().readLock();
        readLock.lock();
        try {
            for (ConexaoServer conexao : lista) {
                if (conexao.getReference().getId() == id) {
                    return conexao;
                }
            }
            return null;
        } finally {
            readLock.unlock();
        }
    }

    public ReadWriteLock getReadWriteLock() {
        return this.lista.getReadWriteLock();
    }

    public ArrayList<ConexaoServer> copy() {
        Lock readLock = this.lista.getReadWriteLock().readLock();
        readLock.lock();
        try {
            return new ArrayList<>(lista);
        } finally {
            readLock.unlock();
        }
    }

    public CloseableIterator<ConexaoServer> iterator() {
        return new LockedIterator<>(this.lista.getReadWriteLock().readLock(), this.lista.iterator());
    }

    public void dispose() {
        this.mapa.dispose();
        this.lista.dispose();
    }
}
