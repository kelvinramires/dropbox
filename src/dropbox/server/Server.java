/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.server;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.util.concurrent.Lock;
import dropbox.client.AbstractProtocoloCliente;
import dropbox.db.ConexaoInfo;
import dropbox.db.ConnectionPool;
import dropbox.db.dao.ArquivoDAO;
import dropbox.db.dao.DAOFactory;
import dropbox.db.model.Arquivo;
import dropbox.db.model.Servidor;
import dropbox.db.model.Usuario;
import dropbox.errors.ArquivoJaExisteException;
import dropbox.errors.ArquivoNaoExisteException;
import dropbox.errors.UsuarioJaExisteException;
import dropbox.errors.UsuarioNaoExisteException;
import dropbox.util.CloseableIterator;
import dropbox.util.NoLocalAddressServerSocketFactory;
import dropbox.util.Predicate;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kelvin
 */
public class Server {

    private static final Logger logger = Logger.getLogger(Server.class.getName());
    public static final String RMI_SERVER_NAME = "Servidor";
    public static final String RMI_CLIENT_NAME = "Cliente";

    private int replicacaoMinima;
    private final ListaDeConexoes conexoes;
    private final ConnectionPool db;
    private final Path diretorio;
    private final Registry registry;
    private final ServerReference localReference;
    private final ScheduledExecutorService threadPool;
    private boolean isClosed;

    public Server(int id, String host, int porta, Path diretorio, ConnectionPool db)
            throws RemoteException, SQLException {
        this(id, host, porta, diretorio, db, LocateRegistry.createRegistry(porta, null, new NoLocalAddressServerSocketFactory()));
    }

    public Server(int id, String host, int porta, Path diretorio, ConnectionPool db, Registry registry)
            throws SQLException, RemoteException {
        this.localReference = new ServerReference(host, porta, id);
        this.replicacaoMinima = 1;
        this.diretorio = diretorio;
        this.db = db;
        this.conexoes = new ListaDeConexoes();
        this.registry = registry;
        this.registry.rebind(RMI_SERVER_NAME, new ProtocoloServerImpl());
        this.registry.rebind(RMI_CLIENT_NAME, new ProtocoloClienteImpl());
        this.threadPool = Executors.newScheduledThreadPool(3);
        this.criarServidorNoDB(this.localReference);
    }

    /**
     * Get the value of diretorio
     *
     * @return the value of diretorio
     */
    public Path getDiretorio() {
        return diretorio;
    }

    /**
     * Get the value of replicacaoMinima
     *
     * @return the value of replicacaoMinima
     */
    public int getReplicacaoMinima() {
        return replicacaoMinima;
    }

    /**
     * Set the value of replicacaoMinima
     *
     * @param replicacaoMinima new value of replicacaoMinima
     */
    public void setReplicacaoMinima(int replicacaoMinima) {
        this.replicacaoMinima = replicacaoMinima;
    }

    /**
     * Get the value of conexoes
     *
     * @return the value of conexoes
     */
    public EventList<ConexaoServer> getConexoes() {
        return conexoes.getReadOnlyView();
    }

    public String getHost() {
        return localReference.getHost();
    }

    public int getPorta() {
        return localReference.getPorta();
    }

    public boolean temOArquivo(String login, String caminho) throws SQLException, IOException {
        try (DAOFactory dao = db.getDAOFactory()) {
            Arquivo arquivo = dao.arquivo().find(login, caminho);
            return arquivo != null;
        }
    }
    
    public boolean temOArquivoLocal(String login, String caminho) throws SQLException, IOException {
        try (DAOFactory dao = db.getDAOFactory()) {
            Arquivo arquivo = dao.arquivo().find(login, caminho);
            if (arquivo == null) {
                throw new IOException("não existe o arquivo");
            }
            if (dao.arquivo().estaNoServidor(arquivo, localReference.getId())) {
                return arquivo.existe(this.diretorio);
            } else {
                return false;
            }
        }
    }

    /**
     * Conecta-se a um outro servidor.
     *
     * @param host host onde o outro servidor está localizado
     * @param port porta onde o outro servidor está localizado
     *
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws NotBoundException quando o outro servidor não foi localizado
     * @throws SQLException quando há algum erro inesperado de SQL
     */
    public void conectar(String host, int port)
            throws RemoteException, NotBoundException, SQLException {
        ProtocoloServer protocolo = (ProtocoloServer) LocateRegistry.getRegistry(host, port).lookup(RMI_SERVER_NAME);
        ConexaoServer conexao = new ConexaoServerImpl(new ProtocoloServerWrapper(protocolo, localReference.getId()));
        List<ServerReference> outrasConexoes = protocolo.conectei(localReference);
        criarServidorNoDB(conexao);
        this.adicionarConexao(conexao);
        for (ServerReference outraConexao : outrasConexoes) {
            if (this.conexoes.findById(outraConexao.getId()) == null) {
                conectar(outraConexao);
            }
        }
    }

    /**
     * Conecta-se a um outro servidor.
     *
     * @param reference host/porta onde o outro servidor está localizado
     *
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws NotBoundException quando o outro servidor não foi localizado
     * @throws SQLException quando há algum erro inesperado de SQL
     */
    public void conectar(ServerReference reference)
            throws RemoteException, NotBoundException, SQLException {
        this.conectar(reference.getHost(), reference.getPorta());
    }

    /**
     * Adiciona usuário no sistema.
     *
     * @param login o login do usuário a ser inserido
     * @param senha a senha do usuário a ser inserido
     *
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws UsuarioJaExisteException quando o usuário já está cadastrado
     */
    public void adicionarUsuario(final String login, final String senha)
            throws SQLException, RemoteException, UsuarioJaExisteException {
        try (DAOFactory dao = db.getDAOFactory()) {
            Usuario usuario = new Usuario();
            usuario.setLogin(login);
            usuario.setSenha(senha);
            dao.usuario().create(usuario);
        }

        try (CloseableIterator<ConexaoServer> iterator = conexoes.iterator()) {
            for (ConexaoServer conexao : iterator) {
                conexao.getProtocolo().adicionarUsuario(login, senha);
            }
        }
    }

    /**
     * Lê um arquivo.
     *
     * @param usuario o login do usuário
     * @param caminho o caminho para o arquivo
     * @return os bytes do arquivo
     *
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws IOException quando há algum erro inesperado de entrada e saída
     */
    public byte[] lerArquivo(String usuario, String caminho)
            throws SQLException, IOException, ArquivoNaoExisteException {
        try (DAOFactory dao = db.getDAOFactory()) {
            Arquivo arquivo = dao.arquivo().find(usuario, caminho);
            if (arquivo == null) {
                throw new ArquivoNaoExisteException();
            }
            if (dao.arquivo().estaNoServidor(arquivo, this.localReference.getId())) {
                return lerArquivoLocal(arquivo);
            } else {
                int idRemoto = dao.arquivo().algumServidorCom(arquivo);
                ConexaoServer conexao = conexoes.findById(idRemoto);
                if (conexao != null) {
                    logger.log(Level.INFO, "pedindo arquivo pro server {0}", idRemoto);
                    return conexao.getProtocolo().lerArquivo(arquivo);
                } else {
                    return null;
                }
            }
        }
    }

    /**
     * Adiciona um arquivo.
     *
     * @param usuario o login do usuário
     * @param caminho o caminho para o arquivo
     * @param dados os bytes do arquivo
     *
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws IOException quando há algum erro inesperado de entrada e saída
     * @throws ArquivoJaExisteException quando o arquivo já existe
     */
    public void adicionarArquivo(String usuario, String caminho, byte[] dados)
            throws SQLException, RemoteException, IOException, ArquivoJaExisteException {
        Arquivo arquivo = new Arquivo();
        try (DAOFactory dao = db.getDAOFactory()) {
            arquivo.setEscrevendo(false);
            arquivo.setLendo(false);
            arquivo.setCaminho(caminho);
            arquivo.setUsuarioLogin(usuario);
            dao.arquivo().create(arquivo);
            try (OutputStream output = arquivo.abrirParaEscrita(diretorio)) {
                output.write(dados);
            }
            dao.arquivo().associar(arquivo, localReference.getId());
            replicar(dao, arquivo, dados, replicacaoMinima);
        }
    }
    
    private class Replicacao implements Runnable {
        private final Arquivo arquivo;
        private final Predicate<ConexaoServer> filtro;
        private final boolean enviarArquivoAntesDosDados;
        private DAOFactory dao;
        private byte[] dados;
        private int copiasAEnviar;
        
        public Replicacao(DAOFactory dao, Arquivo arquivo, byte[] dados, int copiarAEnviar, Predicate<ConexaoServer> filtro, boolean enviarArquivoAntesDosDados) {
            this.dados = dados;
            this.copiasAEnviar = copiarAEnviar;
            this.dao = dao;
            this.arquivo = arquivo;
            this.filtro = filtro;
            this.enviarArquivoAntesDosDados = enviarArquivoAntesDosDados;
        }
        
        @Override
        public void run() {
            try {
                if (dados == null) {
                    dados = lerArquivoLocal(arquivo);
                }
                if (dao == null) {
                    try (DAOFactory newDAO = db.getDAOFactory()) {
                        dao = newDAO;
                        replicar();
                    } finally {
                        dao = null;
                    }
                } else {
                    replicar();
                }
            } catch (SQLException | IOException ex) {
                logger.log(Level.SEVERE, "erro ao executar replicação do arquivo" + arquivo.getId(), ex);
            }
        }

        private void replicar() throws SQLException {
            Lock readLock = conexoes.getReadWriteLock().readLock();
            readLock.lock();
            try {
                ArrayList<ConexaoServer> copia = conexoes.copy();
                Collections.shuffle(copia);
                if (enviarArquivoAntesDosDados) {
                    for (Iterator<ConexaoServer> it = copia.iterator(); it.hasNext();) {
                        ConexaoServer conexao = it.next();
                        if (!enviarArquivo(conexao)) {
                            it.remove();
                        }
                    }
                }
                if (this.filtro != null) {
                    for (Iterator<ConexaoServer> it = copia.iterator(); it.hasNext();) {
                        ConexaoServer conexao = it.next();
                        if (!this.filtro.accept(conexao)) {
                            it.remove();
                        }
                    }
                }
                for (Iterator<ConexaoServer> it = copia.iterator(); it.hasNext() && copiasAEnviar > 0;) {
                    if (enviarDados(it.next())) {
                        copiasAEnviar--;
                    }
                }
            } finally {
                readLock.unlock();
            }
        }

        private boolean enviarDados(ConexaoServer conexao) throws SQLException {
            logger.log(Level.INFO, "enviando dados pro servidor {0}", conexao.getReference().getId());
            try {
                conexao.getProtocolo().enviarArquivo(arquivo, dados);
            } catch (RemoteException ex) {
                logger.log(Level.SEVERE, "erro ao enviar dados pro servidor " + conexao.getReference().getId(), ex);
                return false;
            }
            dao.arquivo().associar(arquivo, conexao.getReference().getId());
            return true;
        }

        private boolean enviarArquivo(ConexaoServer conexao) {
            logger.log(Level.INFO, "enviando arquivo pro servidor {0}", conexao.getReference().getId());
            try {
                conexao.getProtocolo().adicionarArquivo(arquivo);
            } catch (RemoteException ex) {
                logger.log(Level.SEVERE, "erro ao enviar arquivo pro servidor " + conexao.getReference().getId(), ex);
                return false;
            }
            return true;
        }
    }

    private void replicar(DAOFactory dao, Arquivo arquivo, byte[] dados, int replicacao) throws RemoteException, SQLException {
        new Replicacao(dao, arquivo, dados, replicacao, null, true).run();
    }

    /**
     * Substitui um arquivo.
     *
     * @param usuario o login do usuário
     * @param caminho o caminho para o arquivo
     * @param dados os bytes do arquivo
     *
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws IOException quando há algum erro inesperado de entrada e saída
     */
    public void substituirArquivo(String usuario, String caminho, byte[] dados)
            throws RemoteException, SQLException, IOException, ArquivoNaoExisteException {
        this.removerArquivo(usuario, caminho);
        try {
            this.adicionarArquivo(usuario, caminho, dados);
        } catch (ArquivoJaExisteException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    public void copiarArquivo(String usuario, String caminhoOriginal, String caminhoNovo)
            throws RemoteException, SQLException, IOException, ArquivoNaoExisteException, ArquivoJaExisteException {
        byte[] dados = this.lerArquivo(usuario, caminhoOriginal);
        this.adicionarArquivo(usuario, caminhoNovo, dados);
    }

    /**
     * Move um arquivo.
     *
     * @param usuario o login do usuário
     * @param caminhoAntigo o caminho para o arquivo
     * @param caminhoNovo o caminho para a nova localização do arquivo
     *
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws IOException quando há algum erro inesperado de entrada e saída
     */
    public void moverArquivo(String usuario, String caminhoAntigo, String caminhoNovo)
            throws RemoteException, SQLException, IOException, ArquivoNaoExisteException, ArquivoJaExisteException {
        byte[] dados = this.lerArquivo(usuario, caminhoAntigo);
        this.removerArquivo(usuario, caminhoAntigo);
        this.adicionarArquivo(usuario, caminhoNovo, dados);
    }

    public void removerArquivo(String usuario, String caminho) throws SQLException, IOException, ArquivoNaoExisteException {
        Arquivo arquivo;
        try (DAOFactory dao = db.getDAOFactory()) {    
            arquivo = dao.arquivo().find(usuario, caminho);
            if (arquivo == null) {
                throw new ArquivoNaoExisteException();
            }
            if (dao.arquivo().estaNoServidor(arquivo, localReference.getId())) {
                Files.deleteIfExists(arquivo.getPath(diretorio));
            }
            dao.arquivo().delete(arquivo);
        }
        try (CloseableIterator<ConexaoServer> iterator = conexoes.iterator()) {
            for (ConexaoServer conexao : iterator) {
                conexao.getProtocolo().removerArquivo(arquivo);
            }
        }
    }

    /**
     * Para este server.
     */
    public void stop() throws RemoteException, NotBoundException {
        if (this.isClosed) {
            return;
        }
        this.isClosed = true;
        this.registry.unbind("Servidor");
        this.threadPool.shutdownNow();
        this.conexoes.dispose();
    }

    /**
     * Para este server.
     */
    void stopParaTeste() throws RemoteException, NotBoundException {
        if (this.isClosed) {
            return;
        }
        this.isClosed = true;
//        this.registry.unbind("Servidor");
        this.threadPool.shutdownNow();
        this.conexoes.dispose();
    }

    private void criarServidorNoDB(ConexaoServer conexao) throws SQLException {
        criarServidorNoDB(conexao.getReference());
    }

    private void criarServidorNoDB(ServerReference reference) throws SQLException {
        try (DAOFactory dao = db.getDAOFactory()) {
            dao.servidor().create(new Servidor(reference));
        }
    }

    private byte[] lerArquivoLocal(Arquivo arquivo) throws IOException {
        return Files.readAllBytes(arquivo.getPath(diretorio));
    }

    private void adicionarConexao(final ConexaoServer conexao) {
        this.conexoes.add(conexao);
        conexao.scheduleHeartBeat();
    }

    public ServerReference getReference() {
        return this.localReference;
    }

    public ConexaoInfo getDatabaseInfo() {
        return this.db.getInfo();
    }

    private void agendarReplicacao() {
        logger.info("agendando replicação");
        this.threadPool.execute(new Runnable() {

            @Override
            public void run() {

                try {
                    executarReplicacao();
                } catch (SQLException | IOException ex) {
                    logger.log(Level.SEVERE, "Erro ao executar replicação", ex);
                }
            }

        });
    }

    private void executarReplicacao() throws SQLException, IOException {
        logger.info("executando replicação");
        try (DAOFactory dao = this.db.getDAOFactory()) {
            List<ArquivoDAO.PacoteReplicacao> pacotes = dao.arquivo().replicacao(this.localReference.getId(), this.replicacaoMinima);
            for (final ArquivoDAO.PacoteReplicacao pacote : pacotes) {
                int replicacao = replicacaoMinima - pacote.getCount() + 1;
                this.threadPool.submit(new Replicacao(null, pacote.getArquivo(), null, replicacao, new Predicate<ConexaoServer>() {
                    @Override
                    public boolean accept(ConexaoServer conexao) {
                        return pacote.getServidores().contains(conexao.getReference().getId());
                    }
                }, false));
            }
        }
    }

    private class ProtocoloClienteImpl extends AbstractProtocoloCliente {

        public ProtocoloClienteImpl() throws RemoteException {
            super(Server.this.localReference);
        }

        @Override
        public void adicionarUsuario(String usuario, String senha) throws SQLException, RemoteException, UsuarioJaExisteException {
            Server.this.adicionarUsuario(usuario, senha);
        }

        @Override
        public boolean login(String login, String senha) throws SQLException, UsuarioNaoExisteException {
            try (DAOFactory dao = Server.this.db.getDAOFactory()) {
                Usuario usuario = dao.usuario().findByLogin(login);
                return usuario.getSenha().equals(senha);
            }
        }

        @Override
        public List<Arquivo> listar(String usuario) throws SQLException {
            try (DAOFactory dao = new DAOFactory(Server.this.db.getConnection())) {
                return dao.arquivo().allFromUsuario(usuario);
            }
        }

        @Override
        public byte[] lerArquivo(String usuario, String caminho) throws SQLException, IOException, ArquivoNaoExisteException {
            return Server.this.lerArquivo(usuario, caminho);
        }

        @Override
        public List<ServerReference> getServidoresConhecidos() {
            List<ServerReference> servidores = new ArrayList<>();
            try (CloseableIterator<ConexaoServer> iterator = conexoes.iterator()) {
                for (ConexaoServer servidor : iterator) {
                    servidores.add(servidor.getReference());
                }
            }
            return servidores;
        }

        @Override
        public void adicionarArquivo(String usuario, String caminho, byte[] dados)
                throws RemoteException, SQLException, IOException, ArquivoJaExisteException {
            Server.this.adicionarArquivo(usuario, caminho, dados);
        }

        @Override
        public void substituirArquivo(String usuario, String caminho, byte[] dados)
                throws RemoteException, SQLException, IOException, ArquivoNaoExisteException {
            Server.this.substituirArquivo(usuario, caminho, dados);
        }

        @Override
        public void moverArquivo(String usuario, String caminhoAntigo, String caminhoNovo)
                throws RemoteException, SQLException, IOException, ArquivoNaoExisteException, ArquivoJaExisteException {
            Server.this.moverArquivo(usuario, caminhoAntigo, caminhoNovo);
        }

        @Override
        public void copiarArquivo(String usuario, String caminhoOriginal, String caminhoNovo)
                throws RemoteException, SQLException, IOException, ArquivoNaoExisteException, ArquivoJaExisteException {
            Server.this.copiarArquivo(usuario, caminhoOriginal, caminhoNovo);
        }

        @Override
        public void heartBeat() throws RemoteException {
            if (Server.this.isClosed) {
                throw new IllegalStateException("já morri");
            }
        }

        @Override
        public void removerArquivo(String usuario, String caminho) throws RemoteException, SQLException, IOException, ArquivoNaoExisteException {
            Server.this.removerArquivo(usuario, caminho);
        }
    }

    private class ProtocoloServerImpl extends AbstractProtocoloServer implements ProtocoloServer {

        public ProtocoloServerImpl() throws RemoteException {
            super(Server.this.localReference);
        }

        @Override
        public void adicionarUsuario(int serverId, String login, String senha) throws RemoteException {
            this.recebeuHeartBeat(serverId);
            try (DAOFactory dao = db.getDAOFactory()) {
                Usuario usuario = new Usuario();
                usuario.setLogin(login);
                usuario.setSenha(senha);
                dao.usuario().create(usuario);
            } catch (UsuarioJaExisteException | SQLException ex) {
                throw new RemoteException("erro!", ex);
            }
        }

        @Override
        public void adicionarArquivo(int serverId, Arquivo arquivo) throws RemoteException {
            this.recebeuHeartBeat(serverId);
            arquivo.setLendo(false);
            arquivo.setEscrevendo(false);
            try (DAOFactory dao = db.getDAOFactory()) {
                dao.arquivo().create(arquivo);
                dao.arquivo().associar(arquivo, serverId);
            } catch (SQLException | ArquivoJaExisteException ex) {
                throw new RemoteException("erro!", ex);
            }
        }

        @Override
        public void adicionarArquivo(int serverId, Arquivo arquivo, byte[] dados) throws RemoteException {
            this.recebeuHeartBeat(serverId);
            arquivo.setLendo(false);
            arquivo.setEscrevendo(false);
            try (DAOFactory dao = db.getDAOFactory()) {
                dao.arquivo().create(arquivo);
                dao.arquivo().associar(arquivo, serverId);
                try (OutputStream output = arquivo.abrirParaEscrita(diretorio)) {
                    output.write(dados);
                }
                dao.arquivo().associar(arquivo, localReference.getId());
            } catch (SQLException | IOException | ArquivoJaExisteException ex) {
                throw new RemoteException("erro!", ex);
            }
            try (CloseableIterator<ConexaoServer> iterator = conexoes.iterator()) {
                for (ConexaoServer conexao : iterator) {
                    if (conexao.getReference().getId() != serverId) {
                        conexao.getProtocolo().associarArquivo(arquivo);
                    }
                }
            }
        }

        @Override
        public void associarArquivo(int serverId, Arquivo arquivo) throws RemoteException {
            this.recebeuHeartBeat(serverId);
            arquivo.setLendo(false);
            arquivo.setEscrevendo(false);
            try (DAOFactory dao = db.getDAOFactory()) {
                dao.arquivo().associar(arquivo, serverId);
            } catch (SQLException ex) {
                throw new RemoteException("erro!", ex);
            }
        }

        @Override
        public byte[] lerArquivo(int serverId, Arquivo arquivo)
                throws RemoteException {
            this.recebeuHeartBeat(serverId);
            try {
                return lerArquivoLocal(arquivo);
            } catch (IOException ex) {
                throw new RemoteException("erro!", ex);
            }
        }

        @Override
        public void removerArquivo(int serverId, Arquivo arquivo) {
            this.recebeuHeartBeat(serverId);
            try (DAOFactory dao = db.getDAOFactory()) {
                if (dao.arquivo().estaNoServidor(arquivo, localReference.getId())) {
                    Files.deleteIfExists(arquivo.getPath(diretorio));
                }
                dao.arquivo().delete(arquivo);
            } catch (SQLException | IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public List<ServerReference> conectei(ServerReference remoteReference)
                throws RemoteException, AccessException {
            try {
                Registry registry = LocateRegistry.getRegistry(remoteReference.getHost(), remoteReference.getPorta());
                ProtocoloServer protocolo = (ProtocoloServer) registry.lookup(RMI_SERVER_NAME);
                List<ServerReference> outrasConexoes = new ArrayList<>();
                try (CloseableIterator<ConexaoServer> iterator = conexoes.iterator()) {
                    for (ConexaoServer conexao : iterator) {
                        outrasConexoes.add(conexao.getInfo());
                    }
                }
                Server.this.adicionarConexao(new ConexaoServerImpl(remoteReference, new ProtocoloServerWrapper(protocolo, localReference.getId())));
                criarServidorNoDB(remoteReference);
                return outrasConexoes;
            } catch (NotBoundException | SQLException ex) {
                throw new RemoteException("erro!", ex);
            }
        }

        @Override
        protected void recebeuHeartBeat(int serverId) {
            if (Server.this.isClosed) {
                throw new IllegalStateException("já morri!");
            }
            ConexaoServer conexao = Server.this.conexoes.findById(serverId);
            if (conexao != null) {
                conexao.recebeuHeartBeat();
            }
        }

        @Override
        public void enviarArquivo(int serverId, Arquivo arquivo, byte[] dados) throws RemoteException {
            this.recebeuHeartBeat(serverId);
            arquivo.setLendo(false);
            arquivo.setEscrevendo(false);
            try (DAOFactory dao = db.getDAOFactory()) {
                try (OutputStream output = arquivo.abrirParaEscrita(diretorio)) {
                    output.write(dados);
                }
                dao.arquivo().associar(arquivo, localReference.getId());
            } catch (SQLException | IOException ex) {
                throw new RemoteException("erro!", ex);
            }
            try (CloseableIterator<ConexaoServer> iterator = conexoes.iterator()) {
                for (ConexaoServer conexao : iterator) {
                    if (conexao.getReference().getId() != serverId) {
                        conexao.getProtocolo().associarArquivo(arquivo);
                    }
                }
            }
        }
    }

    private class ConexaoServerImpl extends ConexaoServer {

        public ConexaoServerImpl(ProtocoloServerWrapper protocolo) throws RemoteException {
            super(threadPool, protocolo);
        }

        public ConexaoServerImpl(ServerReference reference, ProtocoloServerWrapper protocolo) {
            super(threadPool, reference, protocolo);
        }

        @Override
        protected void morreu() {
            Lock writeLock = conexoes.getReadWriteLock().writeLock();
            writeLock.lock();
            try {
                conexoes.remove(this);
                try (DAOFactory dao = db.getDAOFactory()) {
                    dao.servidor().delete(getReference().getId());
                } catch (SQLException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            } finally {
                writeLock.unlock();
            }
            Server.this.agendarReplicacao();
        }
    }
}
