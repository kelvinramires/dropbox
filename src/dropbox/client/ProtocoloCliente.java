/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.client;

import dropbox.db.model.Arquivo;
import dropbox.errors.ArquivoJaExisteException;
import dropbox.errors.ArquivoNaoExisteException;
import dropbox.errors.UsuarioJaExisteException;
import dropbox.errors.UsuarioNaoExisteException;
import dropbox.server.ServerReference;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Arthur
 */
public interface ProtocoloCliente extends Remote {

    public int getId() throws RemoteException;

    public String getHost() throws RemoteException;

    public int getPort() throws RemoteException;

    /**
     * Lista os servidores conhecidos.
     * @return os servidores conhecidos
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     */
    public List<ServerReference> getServidoresConhecidos() throws RemoteException;

    /**
     * Adiciona usuário no sistema.
     *
     * @param login o login do usuário a ser inserido
     * @param senha a senha do usuário a ser inserido
     *
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws UsuarioJaExisteException quando o usuário já está cadastrado
     */
    public void adicionarUsuario(String login, String senha)
            throws RemoteException, SQLException, UsuarioJaExisteException;

    /**
     * Faz "login" do usuário. Este método não guarda estado, apenas valida as
     * informações de login
     *
     * @param usuario o login do usuário
     * @param senha a senha do usuário
     * @return <code>true</code> se a senha estiver correta e <code>false</code>
     * caso contrário
     *
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws UsuarioNaoExisteException quando o usuário requisitado não existe
     * no sistema
     */
    public boolean login(String usuario, String senha)
            throws RemoteException, SQLException, UsuarioNaoExisteException;

    public List<Arquivo> listar(String usuario)
            throws RemoteException, SQLException;

    /**
     * Lê um arquivo. 
     * @param usuario o login do usuário
     * @param caminho o caminho para o arquivo
     * @return os bytes do arquivo
     * 
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws IOException quando há algum erro inesperado de entrada e saída
     * @throws ArquivoNaoExisteException quando o arquivo não existe
     */
    public byte[] lerArquivo(String usuario, String caminho)
            throws RemoteException, SQLException, IOException, ArquivoNaoExisteException;

    /**
     * Adiciona um arquivo.
     * 
     * @param usuario o login do usuário
     * @param caminho o caminho para o arquivo
     * @param dados os bytes do arquivo
     * 
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws IOException quando há algum erro inesperado de entrada e saída
     * @throws ArquivoJaExisteException quando o arquivo já existe
     */
    public void adicionarArquivo(String usuario, String caminho, byte[] dados)
            throws RemoteException, SQLException, IOException, ArquivoJaExisteException;
    
    /**
     * Substitui um arquivo.
     * 
     * @param usuario o login do usuário
     * @param caminho o caminho para o arquivo
     * @param dados os bytes do arquivo

     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws IOException quando há algum erro inesperado de entrada e saída
     * @throws ArquivoNaoExisteException quando o arquivo não existe
     */
    public void substituirArquivo(String usuario, String caminho, byte[] dados)
            throws RemoteException, SQLException, IOException, ArquivoNaoExisteException;

    /**
     * Move um arquivo.
     * 
     * @param usuario o login do usuário
     * @param caminhoAntigo o caminho para o arquivo
     * @param caminhoNovo o caminho para a nova localização do arquivo
     * 
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws IOException quando há algum erro inesperado de entrada e saída
     * @throws ArquivoNaoExisteException quando o arquivo de origem não existe
     * @throws ArquivoJaExisteException quando o arquivo de destino não existe
     */
    public void moverArquivo(String usuario, String caminhoAntigo, String caminhoNovo)
            throws RemoteException, SQLException, IOException, ArquivoNaoExisteException, ArquivoJaExisteException;

    /**
     * Copia um arquivo.
     * 
     * @param usuario o login do usuário
     * @param caminhoOriginal o caminho para o arquivo
     * @param caminhoNovo o caminho para a nova localização do arquivo
     * 
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws IOException quando há algum erro inesperado de entrada e saída
     * @throws ArquivoNaoExisteException quando o arquivo de origem não existe
     * @throws ArquivoJaExisteException quando o arquivo de destino não existe
     */
    public void copiarArquivo(String usuario, String caminhoOriginal, String caminhoNovo)
            throws RemoteException, SQLException, IOException, ArquivoNaoExisteException, ArquivoJaExisteException;
    
    
    /**
     * Remover um arquivo.
     * 
     * @param usuario o login do usuário
     * @param caminho caminho para o arquivo
     * 
     * @throws RemoteException quando há algum erro inesperado de comunicação do
     * RMI
     * @throws SQLException quando há algum erro inesperado de SQL
     * @throws IOException quando há algum erro inesperado de entrada e saída
     * @throws ArquivoNaoExisteException quando o arquivo não existe
     */
    public void removerArquivo(String usuario, String caminho)
            throws RemoteException, SQLException, IOException, ArquivoNaoExisteException;


    public void heartBeat() throws RemoteException;
}
