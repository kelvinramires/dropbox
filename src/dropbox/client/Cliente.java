/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.client;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.util.concurrent.Lock;
import dropbox.db.model.Arquivo;
import dropbox.errors.ArquivoJaExisteException;
import dropbox.errors.ArquivoNaoExisteException;
import dropbox.errors.UsuarioJaExisteException;
import dropbox.errors.UsuarioNaoExisteException;
import dropbox.server.Server;
import dropbox.server.ServerReference;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.SwingPropertyChangeSupport;

/**
 *
 * @author Arthur
 */
public class Cliente {

    private static final Logger logger = Logger.getLogger(Cliente.class.getName());

    private final EventList<ServerReference> servidores;
    private final EventList<Arquivo> arquivos;
    private final ScheduledExecutorService threadPool;
    private String usuarioLogin;
    private Conexao conexao;
    private ScheduledFuture<?> sendHeartBeatFuture;
    private ScheduledFuture<?> waitHeartBeatFuture;

    private boolean conectado;

    public static final String PROP_CONECTADO = "conectado";

    private boolean logado;

    public static final String PROP_LOGADO = "logado";

    /**
     * Get the value of logado
     *
     * @return the value of logado
     */
    public boolean isLogado() {
        return logado;
    }

    /**
     * Set the value of logado
     *
     * @param logado new value of logado
     */
    public void setLogado(boolean logado) {
        boolean oldLogado = this.logado;
        this.logado = logado;
        propertyChangeSupport.firePropertyChange(PROP_LOGADO, oldLogado, logado);
    }

    /**
     * Get the value of conectado
     *
     * @return the value of conectado
     */
    public boolean isConectado() {
        return conectado;
    }

    /**
     * Set the value of conectado
     *
     * @param conectado new value of conectado
     */
    public void setConectado(boolean conectado) {
        boolean oldConectado = this.conectado;
        this.conectado = conectado;
        propertyChangeSupport.firePropertyChange(PROP_CONECTADO, oldConectado, conectado);
    }

    private transient final SwingPropertyChangeSupport propertyChangeSupport = new SwingPropertyChangeSupport(this, true);

    /**
     * Add PropertyChangeListener.
     *
     * @param listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Add PropertyChangeListener.
     *
     * @param propertyName
     * @param listener
     */
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * Remove PropertyChangeListener.
     *
     * @param listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    /**
     * Remove PropertyChangeListener.
     *
     * @param propertyName
     * @param listener
     */
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
    }

    public Cliente() {
        this.conectado = false;
        this.logado = false;
        this.usuarioLogin = null;
        this.servidores = new BasicEventList<>();
        this.arquivos = new BasicEventList<>();
        this.threadPool = Executors.newScheduledThreadPool(3);
    }

    private void conectar(ServerReference reference) throws RemoteException, NotBoundException {
        this.conectar(reference.getHost(), reference.getPorta());
    }

    public void conectar(String host, int porta) throws RemoteException, NotBoundException {
        ProtocoloCliente protocolo = (ProtocoloCliente) LocateRegistry.getRegistry(host, porta).lookup(Server.RMI_CLIENT_NAME);
        this.conexao = new Conexao(protocolo, new ServerReference(protocolo.getHost(), protocolo.getPort(), protocolo.getId()));
        this.servidores.clear();
        this.servidores.add(this.conexao.reference);
        this.servidores.addAll(this.conexao.protocolo.getServidoresConhecidos());
        this.sendHeartBeatFuture = this.threadPool.scheduleWithFixedDelay(new SendHeartBeatRunnable(), 500, 500, TimeUnit.MILLISECONDS);
        this.setConectado(true);
    }

    public void dispose() {
        this.threadPool.shutdownNow();
    }

    public void adicionarUsuario(String usuario, String senha)
            throws RemoteException, SQLException, UsuarioJaExisteException {
        this.conexao.protocolo.adicionarUsuario(usuario, senha);
    }

    public String getUsuarioLogin() {
        return this.usuarioLogin;
    }

    public void login(String usuarioLogin, String senha) throws RemoteException, SQLException, UsuarioNaoExisteException {
        if (this.usuarioLogin != null) {
            throw new IllegalStateException("Já está logado");
        }
        this.conexao.protocolo.login(usuarioLogin, senha);
        this.usuarioLogin = usuarioLogin;
        this.setLogado(true);
        this.atualizarListaDeArquivos();
    }

    public EventList<ServerReference> getServidores() {
        return GlazedLists.readOnlyList(servidores);
    }

    public EventList<Arquivo> getArquivos() {
        return GlazedLists.readOnlyList(arquivos);
    }

    public void adicionarArquivo(File file, String caminho) throws IOException, RemoteException, SQLException, ArquivoJaExisteException {
        if (this.usuarioLogin == null) {
            throw new IllegalStateException("Não está logado");
        }
        byte[] dados = Files.readAllBytes(file.toPath());
        this.conexao.protocolo.adicionarArquivo(usuarioLogin, caminho, dados);
    }

    public byte[] lerArquivo(String caminho) throws SQLException, IOException, RemoteException, ArquivoNaoExisteException {
        if (this.usuarioLogin == null) {
            throw new IllegalStateException("Não está logado");
        }
        return this.conexao.protocolo.lerArquivo(usuarioLogin, caminho);
    }

    public void removerArquivo(String arquivo) throws SQLException, IOException, RemoteException, ArquivoNaoExisteException {
        if (this.usuarioLogin == null) {
            throw new IllegalStateException("Não está logado");
        }
        this.conexao.protocolo.removerArquivo(usuarioLogin, arquivo);
    }

    public void moverArquivo(String arquivo, String novoCaminho) throws SQLException, IOException, RemoteException, ArquivoNaoExisteException, ArquivoJaExisteException {
        if (this.usuarioLogin == null) {
            throw new IllegalStateException("Não está logado");
        }
        this.conexao.protocolo.moverArquivo(usuarioLogin, arquivo, novoCaminho);
    }

    public void copiarArquivo(String arquivo, String novoCaminho) throws SQLException, IOException, RemoteException, ArquivoNaoExisteException, ArquivoJaExisteException {
        if (this.usuarioLogin == null) {
            throw new IllegalStateException("Não está logado");
        }
        this.conexao.protocolo.copiarArquivo(usuarioLogin, arquivo, novoCaminho);
    }

    public void substituirArquivo(String arquivo, File file) throws IOException, RemoteException, SQLException, ArquivoNaoExisteException {
        if (this.usuarioLogin == null) {
            throw new IllegalStateException("Não está logado");
        }
        byte[] dados = Files.readAllBytes(file.toPath());
        this.conexao.protocolo.substituirArquivo(this.usuarioLogin, arquivo, dados);
    }

    public void atualizarListaDeArquivos() {
        if (this.usuarioLogin == null) {
            throw new IllegalStateException("Não está logado");
        }
        this.threadPool.execute(new Runnable() {
            @Override
            public void run() {
                List<Arquivo> arquivosNovos;
                try {
                    arquivosNovos = conexao.protocolo.listar(usuarioLogin);
                } catch (RemoteException | SQLException ex) {
                    logger.log(Level.SEVERE, null, ex);
                    return;
                }
                Lock writeLock = arquivos.getReadWriteLock().writeLock();
                writeLock.lock();
                try {
                    GlazedLists.replaceAll(arquivos, arquivosNovos, true);
                } finally {
                    writeLock.unlock();
                }
            }
        });
    }

    private void recebeuHeartBeat(Conexao conexao, List<ServerReference> novosServidores) {
        if (conexao != this.conexao) {
            return;
        }
        synchronized (this) {
            if (this.waitHeartBeatFuture != null) {
                this.waitHeartBeatFuture.cancel(false);
                this.waitHeartBeatFuture = null;
            }
            this.waitHeartBeatFuture = this.threadPool.schedule(new WaitHeartBeatRunnable(), 1000, TimeUnit.MILLISECONDS);
        }
        Set<ServerReference> novosServidoresSet = new HashSet<>(novosServidores);
        novosServidoresSet.add(conexao.reference);
        servidores.getReadWriteLock().writeLock().lock();
        try {
            Set<ServerReference> servidoresSet = new HashSet<>(this.servidores);
            if (novosServidoresSet.size() != servidoresSet.size()) {
                while (this.servidores.size() > 1) {
                    this.servidores.remove(1);
                }
                this.servidores.addAll(novosServidores);
            }
        } finally {
            servidores.getReadWriteLock().writeLock().unlock();
        }
    }

    private void naoRecebeuHeartBeat(Conexao conexao) {
        if (conexao != this.conexao) {
            return;
        }
        this.conexao = null;
        logger.info("naoRecebeuHeartBeat");
        if (this.waitHeartBeatFuture != null) {
            this.waitHeartBeatFuture.cancel(false);
            this.waitHeartBeatFuture = null;
        }
        if (this.sendHeartBeatFuture != null) {
            this.sendHeartBeatFuture.cancel(false);
            this.sendHeartBeatFuture = null;
        }
        Lock writeLock = servidores.getReadWriteLock().writeLock();
        writeLock.lock();
        try {
            ServerReference[] copiaServidores = servidores.toArray(new ServerReference[servidores.size()]);
            for (ServerReference servidor : copiaServidores) {
                if (!servidor.equals(conexao.reference)) {
                    try {
                        conectar(servidor);
                        break;
                    } catch (Throwable ex) {
                        logger.log(Level.SEVERE, null, ex);
                    }
                }
            }
        } finally {
            writeLock.unlock();
        }
    }

    private static class Conexao {

        private final ProtocoloCliente protocolo;
        private final ServerReference reference;

        public Conexao(ProtocoloCliente protocolo, ServerReference info) {
            this.protocolo = protocolo;
            this.reference = info;
        }
    }

    private class WaitHeartBeatRunnable implements Runnable {

        private final Conexao conexao;

        public WaitHeartBeatRunnable() {
            this.conexao = Cliente.this.conexao;
        }

        @Override
        public void run() {
            naoRecebeuHeartBeat(conexao);
        }
    }

    private class SendHeartBeatRunnable implements Runnable {

        private final Conexao conexao;

        public SendHeartBeatRunnable() {
            this.conexao = Cliente.this.conexao;
        }

        @Override
        public void run() {
            if (conexao == null) {
                return;
            }
            List<ServerReference> novosServidores;
            try {
                novosServidores = conexao.protocolo.getServidoresConhecidos();
            } catch (Throwable ex) {
                logger.log(Level.SEVERE, null, ex);
                naoRecebeuHeartBeat(this.conexao);
                return;
            }
            recebeuHeartBeat(this.conexao, novosServidores);
        }
    }
}
