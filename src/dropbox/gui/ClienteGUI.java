/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.gui;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.FunctionList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.GlazedListsSwing;
import ca.odell.glazedlists.util.concurrent.Lock;
import com.ezware.dialog.task.TaskDialogs;
import dropbox.client.Cliente;
import dropbox.db.model.Arquivo;
import dropbox.errors.ArquivoJaExisteException;
import dropbox.errors.ArquivoNaoExisteException;
import dropbox.errors.UsuarioJaExisteException;
import dropbox.errors.UsuarioNaoExisteException;
import dropbox.server.ServerReference;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

/**
 *
 * @author Arthur
 */
public class ClienteGUI extends javax.swing.JFrame {

    private static final Logger logger = Logger.getLogger(ClienteGUI.class.getName());
    private final Cliente cliente;

    /**
     * Creates new form ClienteGUI
     */
    public ClienteGUI() {
        initComponents();
        this.cliente = new Cliente();
        this.cliente.addPropertyChangeListener(Cliente.PROP_CONECTADO, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                boolean conectado = (boolean) evt.getNewValue();
                if (conectado) {
                    portaField.setEditable(false);
                    hostField.setEditable(false);
                    conectarButton.setEnabled(false);
                    if (cliente.isLogado()) {
                        logarButton.setEnabled(false);
                        statusLabel.setText("Status: logado como " + cliente.getUsuarioLogin());
                    } else {
                        logarButton.setEnabled(true);
                        statusLabel.setText("Status: não está logado");
                    }
                    adicionarUsuarioButton.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(rootPane, evt);

                    statusLabel.setText("Status: não está conectado");
                    portaField.setEditable(true);
                    hostField.setEditable(true);
                    conectarButton.setEnabled(true);
                    logarButton.setEnabled(false);
                    adicionarUsuarioButton.setEnabled(false);
                }
            }
        });
        this.cliente.addPropertyChangeListener(Cliente.PROP_LOGADO, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                boolean logado = (boolean) evt.getNewValue();
                if (cliente.isConectado()) {
                    if (logado) {
                        logarButton.setEnabled(false);
                        statusLabel.setText("Status: logado como " + cliente.getUsuarioLogin());
                    } else {
                        logarButton.setEnabled(true);
                        statusLabel.setText("Status: não está logado");
                    }
                } else {
                    statusLabel.setText("Status: não está conectado");
                    logarButton.setEnabled(false);
                }
            }
        });
        this.tabelaDeServidores.setModel(GlazedListsSwing.eventTableModelWithThreadProxyList(this.cliente.getServidores(), new TableFormatImpl()));
        this.listaDeArquivos.setModel(GlazedListsSwing.eventListModelWithThreadProxyList(GlazedLists.transformByFunction(this.cliente.getArquivos(), new AdvancedFunctionImpl())));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        abrirArquivoChooser = new javax.swing.JFileChooser();
        salvarArquivoChooser = new javax.swing.JFileChooser();
        conectarButton = new javax.swing.JButton();
        hostField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        portaField = new javax.swing.JFormattedTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabelaDeServidores = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        listaDeArquivos = new javax.swing.JList();
        jToolBar1 = new javax.swing.JToolBar();
        adicionarArquivoButton = new javax.swing.JButton();
        baixarArquivoButton = new javax.swing.JButton();
        removerArquivoButton = new javax.swing.JButton();
        moverArquivoButton = new javax.swing.JButton();
        substituirArquivoButton = new javax.swing.JButton();
        copiarArquivoButton = new javax.swing.JButton();
        atualizarListaDeArquivosButton = new javax.swing.JButton();
        logarButton = new javax.swing.JButton();
        adicionarUsuarioButton = new javax.swing.JButton();
        statusLabel = new javax.swing.JLabel();

        abrirArquivoChooser.setMultiSelectionEnabled(true);

        salvarArquivoChooser.setDialogType(javax.swing.JFileChooser.SAVE_DIALOG);
        salvarArquivoChooser.setApproveButtonText("Salvar");
        salvarArquivoChooser.setApproveButtonToolTipText("");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Cliente");

        conectarButton.setText("Conectar");
        conectarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                conectarButtonActionPerformed(evt);
            }
        });

        hostField.setText("localhost");

        jLabel1.setText("Host");

        jLabel2.setText("Porta");

        portaField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));

        tabelaDeServidores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tabelaDeServidores);

        listaDeArquivos.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(listaDeArquivos);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        adicionarArquivoButton.setText("Adicionar");
        adicionarArquivoButton.setFocusable(false);
        adicionarArquivoButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        adicionarArquivoButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        adicionarArquivoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicinarArquivoAction(evt);
            }
        });
        jToolBar1.add(adicionarArquivoButton);

        baixarArquivoButton.setText("Baixar");
        baixarArquivoButton.setFocusable(false);
        baixarArquivoButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        baixarArquivoButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        baixarArquivoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                baixarArquivoButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(baixarArquivoButton);

        removerArquivoButton.setText("Remover");
        removerArquivoButton.setFocusable(false);
        removerArquivoButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        removerArquivoButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        removerArquivoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removerArquivoButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(removerArquivoButton);

        moverArquivoButton.setText("Mover");
        moverArquivoButton.setFocusable(false);
        moverArquivoButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        moverArquivoButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        moverArquivoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moverArquivoButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(moverArquivoButton);

        substituirArquivoButton.setText("Substituir");
        substituirArquivoButton.setFocusable(false);
        substituirArquivoButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        substituirArquivoButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        substituirArquivoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                substituirArquivoButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(substituirArquivoButton);

        copiarArquivoButton.setText("Copiar");
        copiarArquivoButton.setFocusable(false);
        copiarArquivoButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        copiarArquivoButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        copiarArquivoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copiarArquivoButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(copiarArquivoButton);

        atualizarListaDeArquivosButton.setText("Atualizar");
        atualizarListaDeArquivosButton.setFocusable(false);
        atualizarListaDeArquivosButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        atualizarListaDeArquivosButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        atualizarListaDeArquivosButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atualizarListaDeArquivosButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(atualizarListaDeArquivosButton);

        logarButton.setText("Logar");
        logarButton.setEnabled(false);
        logarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logarButtonActionPerformed(evt);
            }
        });

        adicionarUsuarioButton.setText("Adicionar Usuário");
        adicionarUsuarioButton.setEnabled(false);
        adicionarUsuarioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicionarUsuarioButtonActionPerformed(evt);
            }
        });

        statusLabel.setText("Status: não está conectado");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(adicionarUsuarioButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(logarButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(conectarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2))
                                .addGap(34, 34, 34)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(portaField, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(hostField, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(hostField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(portaField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(statusLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(conectarButton)
                            .addComponent(logarButton)
                            .addComponent(adicionarUsuarioButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 307, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void conectarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_conectarButtonActionPerformed
        try {
            this.portaField.commitEdit();
            String host = this.hostField.getText();
            int porta = (int) (long) this.portaField.getValue();
            this.cliente.conectar(host, porta);
        } catch (RemoteException | NotBoundException | ParseException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_conectarButtonActionPerformed

    private void adicinarArquivoAction(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adicinarArquivoAction
        if (this.cliente == null) {
            return;
        }

        if (this.abrirArquivoChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                File[] files = this.abrirArquivoChooser.getSelectedFiles();
                for (File file : files) {
                    String caminho = this.criarDialogo().instruction("instruction").input(file.getName());
                    this.cliente.adicionarArquivo(file, caminho);
                }
            } catch (IOException | SQLException ex) {
                logger.log(Level.SEVERE, null, ex);
                criarDialogo().showException(ex);
            } catch (ArquivoJaExisteException ex) {
                criarDialogo().text("arquivo já existe").instruction("arquivo já existe").error();
            }
        }
    }//GEN-LAST:event_adicinarArquivoAction

    private void atualizarListaDeArquivosButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atualizarListaDeArquivosButtonActionPerformed
        if (this.cliente == null) {
            return;
        }
        this.cliente.atualizarListaDeArquivos();
    }//GEN-LAST:event_atualizarListaDeArquivosButtonActionPerformed

    private void removerArquivoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removerArquivoButtonActionPerformed
        final int selectedIndex = this.listaDeArquivos.getSelectedIndex();
        if (selectedIndex == -1) {
            criarDialogo().text("primeiro, selecione um arquivo").instruction("primeiro, selecione um arquivo").error();
            return;
        }
        String arquivo = (String) this.listaDeArquivos.getModel().getElementAt(selectedIndex);
        try {
            this.cliente.removerArquivo(arquivo);
        } catch (SQLException | IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            criarDialogo().showException(ex);
        } catch (ArquivoNaoExisteException ex) {
            logger.log(Level.SEVERE, null, ex);
            criarDialogo().text("arquivo não existe").instruction("arquivo não existe").error();
        }
    }//GEN-LAST:event_removerArquivoButtonActionPerformed

    private void moverArquivoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moverArquivoButtonActionPerformed
        final int selectedIndex = this.listaDeArquivos.getSelectedIndex();
        if (selectedIndex == -1) {
            criarDialogo().text("primeiro, selecione um arquivo").instruction("primeiro, selecione um arquivo").error();
            return;
        }

        String arquivo = (String) this.listaDeArquivos.getModel().getElementAt(selectedIndex);
        String novoCaminho = criarDialogo().text("Escolha o destino").instruction("Escolha o destino").input(arquivo);
        if (novoCaminho.equals(arquivo)) {
            return;
        }
        try {
            this.cliente.moverArquivo(arquivo, novoCaminho);
        } catch (SQLException | IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            criarDialogo().showException(ex);
        } catch (ArquivoNaoExisteException ex) {
            logger.log(Level.SEVERE, null, ex);
            criarDialogo().text("arquivo não existe").instruction("arquivo não existe").error();
        } catch (ArquivoJaExisteException ex) {
            logger.log(Level.SEVERE, null, ex);
            criarDialogo().text("arquivo já existe").instruction("arquivo já existe").error();
        }
    }//GEN-LAST:event_moverArquivoButtonActionPerformed

    private void substituirArquivoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_substituirArquivoButtonActionPerformed
        final int selectedIndex = this.listaDeArquivos.getSelectedIndex();
        if (selectedIndex == -1) {
            criarDialogo().text("primeiro, selecione um arquivo").instruction("primeiro, selecione um arquivo").error();
            return;
        }

        String arquivo = (String) this.listaDeArquivos.getModel().getElementAt(selectedIndex);

        if (this.abrirArquivoChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                File file = this.abrirArquivoChooser.getSelectedFile();
                this.cliente.substituirArquivo(arquivo, file);
            } catch (IOException | SQLException ex) {
                logger.log(Level.SEVERE, null, ex);
                criarDialogo().showException(ex);
            } catch (ArquivoNaoExisteException ex) {
                criarDialogo().text("arquivo não existe").instruction("arquivo não existe").error();
            }
        }
    }//GEN-LAST:event_substituirArquivoButtonActionPerformed

    private void copiarArquivoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copiarArquivoButtonActionPerformed
        final int selectedIndex = this.listaDeArquivos.getSelectedIndex();
        if (selectedIndex == -1) {
            criarDialogo().text("primeiro, selecione um arquivo").instruction("primeiro, selecione um arquivo").error();
            return;
        }

        String arquivo = (String) this.listaDeArquivos.getModel().getElementAt(selectedIndex);
        String novoCaminho = criarDialogo().text("Escolha o destino").instruction("Escolha o destino").input(arquivo);
        try {
            this.cliente.copiarArquivo(arquivo, novoCaminho);
        } catch (SQLException | IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            criarDialogo().showException(ex);
        } catch (ArquivoNaoExisteException ex) {
            logger.log(Level.SEVERE, null, ex);
            criarDialogo().text("arquivo não existe").instruction("arquivo não existe").error();
        } catch (ArquivoJaExisteException ex) {
            logger.log(Level.SEVERE, null, ex);
            criarDialogo().text("arquivo já existe").instruction("arquivo já existe").error();
        }
    }//GEN-LAST:event_copiarArquivoButtonActionPerformed

    private void baixarArquivoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_baixarArquivoButtonActionPerformed
        final int selectedIndex = this.listaDeArquivos.getSelectedIndex();
        if (selectedIndex == -1) {
            criarDialogo().text("primeiro, selecione um arquivo").instruction("primeiro, selecione um arquivo").error();
            return;
        }
        String arquivo = (String) this.listaDeArquivos.getModel().getElementAt(selectedIndex);
        if (this.salvarArquivoChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                File file = this.salvarArquivoChooser.getSelectedFile();
                Files.write(file.toPath(), this.cliente.lerArquivo(arquivo));
            } catch (IOException | SQLException ex) {
                logger.log(Level.SEVERE, null, ex);
                criarDialogo().showException(ex);
            } catch (ArquivoNaoExisteException ex) {
                criarDialogo().text("arquivo não existe").instruction("arquivo não existe").error();
            }
        }
    }//GEN-LAST:event_baixarArquivoButtonActionPerformed

    private void adicionarUsuarioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adicionarUsuarioButtonActionPerformed
        NewJDialog dialog = new NewJDialog(this, true, new NewJDialog.Callback() {

            @Override
            public void onSuccess(final String username, final String password) {
                new SwingWorker<Void, Void>() {
                    @Override
                    protected Void doInBackground() throws RemoteException, SQLException, UsuarioJaExisteException {
                        cliente.adicionarUsuario(username, password);
                        return null;
                    }

                    @Override
                    protected void done() {
                        super.done();
                        try {
                            this.get();
                        } catch (InterruptedException ex) {
                            logger.log(Level.SEVERE, null, ex);
                        } catch (ExecutionException ex) {
                            logger.log(Level.SEVERE, null, ex);
                            Throwable cause = ex.getCause();
                            if (cause instanceof UsuarioJaExisteException) {
                                criarDialogo().title("Erro").instruction("Usuario já existe").text("").error();
                            } else {
                                criarDialogo().showException(ex);
                            }
                        }
                    }
                }.execute();
            }

            @Override
            public void onFailure(Throwable ex) {

            }
        });
        dialog.setTitle("Adicione um usuário");
        dialog.getButton().setText("Adicionar usuário");
        dialog.setVisible(true);
    }//GEN-LAST:event_adicionarUsuarioButtonActionPerformed

    private void logarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logarButtonActionPerformed
        NewJDialog dialog = new NewJDialog(this, true, new NewJDialog.Callback() {

            @Override
            public void onSuccess(final String username, final String password) {
                new SwingWorker<Void, Void>() {
                    @Override
                    protected Void doInBackground() throws RemoteException, SQLException, UsuarioNaoExisteException {
                        cliente.login(username, password);
                        return null;
                    }

                    @Override
                    protected void done() {
                        super.done();
                        try {
                            this.get();
                        } catch (InterruptedException ex) {
                            logger.log(Level.SEVERE, null, ex);
                        } catch (ExecutionException ex) {
                            logger.log(Level.SEVERE, null, ex);
                            Throwable cause = ex.getCause();
                            if (cause instanceof UsuarioNaoExisteException) {
                                criarDialogo().title("Erro").instruction("Usuario não existe").text("Crie o usuário").error();
                            } else {
                                criarDialogo().showException(ex);
                            }
                        }
                    }
                }.execute();
            }

            @Override
            public void onFailure(Throwable ex) {

            }

        });
        dialog.setTitle("Faça login");
        dialog.getButton().setText("Logar");
        dialog.setVisible(true);
    }//GEN-LAST:event_logarButtonActionPerformed

    private TaskDialogs.TaskDialogBuilder criarDialogo() {
        return TaskDialogs.build().parent(this);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser abrirArquivoChooser;
    private javax.swing.JButton adicionarArquivoButton;
    private javax.swing.JButton adicionarUsuarioButton;
    private javax.swing.JButton atualizarListaDeArquivosButton;
    private javax.swing.JButton baixarArquivoButton;
    private javax.swing.JButton conectarButton;
    private javax.swing.JButton copiarArquivoButton;
    private javax.swing.JTextField hostField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JList listaDeArquivos;
    private javax.swing.JButton logarButton;
    private javax.swing.JButton moverArquivoButton;
    private javax.swing.JFormattedTextField portaField;
    private javax.swing.JButton removerArquivoButton;
    private javax.swing.JFileChooser salvarArquivoChooser;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JButton substituirArquivoButton;
    private javax.swing.JTable tabelaDeServidores;
    // End of variables declaration//GEN-END:variables

    private static class AdvancedFunctionImpl implements FunctionList.AdvancedFunction<Arquivo, String> {

        @Override
        public String reevaluate(Arquivo sourceValue, String transformedValue) {
            return evaluate(sourceValue);
        }

        @Override
        public void dispose(Arquivo sourceValue, String transformedValue) {

        }

        @Override
        public String evaluate(Arquivo sourceValue) {
            return sourceValue.getCaminho();
        }
    }

    private class TableFormatImpl implements TableFormat<ServerReference> {

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return "ID";
                case 1:
                    return "Host:Porta";
                case 2:
                    return "";
            }
            return null;
        }

        @Override
        public Object getColumnValue(ServerReference info, int column) {
            switch (column) {
                case 0:
                    return info.getId();
                case 1:
                    return info.getHostPort();
                case 2:
                    ServerReference firstServer;
                    EventList<ServerReference> servidores = cliente.getServidores();
                    Lock readLock = servidores.getReadWriteLock().readLock();
                    readLock.lock();
                    try {
                        if (servidores.size() > 0) {
                            firstServer = servidores.get(0);
                            if (firstServer.equals(info)) {
                                return "conectado";
                            } else {
                                return "";
                            }
                        } else {
                            return "";
                        }
                    } finally {
                        readLock.unlock();
                    }
            }
            return null;
        }
    }
}
