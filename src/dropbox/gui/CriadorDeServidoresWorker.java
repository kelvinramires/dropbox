/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.gui;

import dropbox.util.Producer;
import com.ezware.dialog.task.TaskDialogs;
import dropbox.db.ConexaoInfo;
import dropbox.db.ConnectionPool;
import dropbox.db.CriacaoDB;
import dropbox.db.dao.DAOFactory;
import dropbox.server.Server;
import dropbox.util.Files2;
import dropbox.util.IPUtil;
import java.awt.Window;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import org.postgresql.util.PSQLException;

/**
 *
 * @author Arthur
 */
public class CriadorDeServidoresWorker extends SwingWorker<Server, String> {
    private String status;
    private final ConnectionPool pool;
    private final ConexaoInfo info;
    private final int id;
    private final int port;
    private final Path directory;
    private boolean poolInUse;
    private Window dialogParent;
    
    public CriadorDeServidoresWorker(int id, int port, ConexaoInfo info, String directory) {
        this.info = info;
        this.pool = new ConnectionPool(this.info);
        this.poolInUse = false;
        this.id = id;
        this.port = port;
        this.directory = FileSystems.getDefault().getPath(directory).toAbsolutePath();
    }
    
    public Window getDialogParent() {
        return dialogParent;
    }
    
    public void setDialogParent(Window dialogParent) {
        this.dialogParent = dialogParent;
    }

    private static <T> T invokeAndWait(Producer<T> producer) throws InterruptedException, InvocationTargetException {
        RunnableImpl<T> runnableImpl1 = new RunnableImpl(producer);
        SwingUtilities.invokeAndWait(runnableImpl1);
        return runnableImpl1.result;
    }

    @Override
    protected Server doInBackground() throws Exception {
        try {
            if (!this.conectarDatabase()) {
                return null;
            }
            if (!this.checarDatabase()) {
                return null;
            }
            InetAddress ip = selecionarIP();
            if (!this.checarDiretorio()) {
                return null;
            }
            Server result = new Server(this.id, ip.getHostAddress(), this.port, this.directory, this.pool);
            this.poolInUse = true;
            return result;
        } finally {
            if (!this.poolInUse) {
                this.pool.close();
            }
        }
    }
            
    private boolean conectarDatabase() throws SQLException, InterruptedException, InvocationTargetException {
        this.publish("Conectando ao database");
        try (Connection connection = info.createConnection()) {
            this.publish("Conectado ao database");
            return true;
        } catch (SQLException ex) {
            if (!isDatabaseNotFound(ex)) {
                throw ex;
            }
            final String title = "Banco de dados " + info.getDatabase() + " não foi encontrado";
            
            if (!invokeAndWait(new Producer<Boolean>() {
                @Override
                public Boolean call() {
                    return criarDialogo()
                            .title(title)
                            .instruction(title + "\nDeseja criar o banco de dados automaticamente?")
                            .ask();
                }
            })) {
                return false;
            }
            this.publish("Criando o database");
            ConexaoInfo infoAdm = info.clone();
            infoAdm.setDatabase("postgres");
            try (Connection connection = infoAdm.createConnection()) {
                new CriacaoDB(connection).criarDatabase(info.getDatabase());
            }
            try (Connection connection = info.createConnection()) {
                new CriacaoDB(connection).criarTabelas();
            }
            
            return true;
        }
    }
    
    private static boolean isDatabaseNotFound(SQLException ex) {
        if (Pattern.matches("FATAL: database \".*\" does not exist", ex.getMessage())) {
            return true;
        }
        if (ex instanceof PSQLException) {
            return ((PSQLException) ex).getServerErrorMessage().getSQLState().equalsIgnoreCase("3D000");
        } else {
            return false;
        }
    }
    
    private boolean checarDatabase() throws SQLException, InterruptedException, InvocationTargetException {
        this.publish("Checando o database");
        try (DAOFactory dao = new DAOFactory(this.pool.getConnection())) {
            int arquivos = dao.arquivo().count();
            int servidores = dao.servidor().count();
            int usuarios = dao.usuario().count();
            if (arquivos == 0 && servidores == 0 && usuarios == 0) {
                return true;
            }
            if (invokeAndWait(new Producer<Boolean>() {
                @Override
                public Boolean call() {
                    return criarDialogo()
                            .title("Database não está vazio")
                            .instruction("Database não está vazio, deseja limpá-lo?")
                            .ask();
                }
            })) {
                dao.servidor().deleteAll();
                dao.arquivo().deleteAll();
                dao.usuario().deleteAll();
                return true;
            } else {
                return false;
            }
        }
    }
    
    private InetAddress selecionarIP() throws UnknownHostException, InterruptedException, InvocationTargetException {
        List<InetAddress> candidateIps = IPUtil.getLocalHostLANAddresses(true);
        if (candidateIps.size() == 1) {
            return candidateIps.get(0);
        } else {
            final List<String> choices = new ArrayList<>(candidateIps.size());
            for (InetAddress candidateIp : candidateIps) {
                choices.add(candidateIp.getHostAddress());
            }
            int choice = invokeAndWait(new Producer<Integer>() {

                @Override
                public Integer call() {
                    return criarDialogo()
                    .title("Escolha o endereço IP")
                    .instruction("Escolha o endereço IP correto para este servidor")
                    .radioChoice(0, choices);
                }
            });
            return candidateIps.get(choice);
        }
    }
    
    private TaskDialogs.TaskDialogBuilder criarDialogo() {
        TaskDialogs.TaskDialogBuilder builder = TaskDialogs.build();
        return this.dialogParent == null ? builder : builder.parent(dialogParent);
    }
        
    private boolean checarDiretorio() throws IOException {
        this.publish("Checando o diretório");
        if (Files.notExists(directory)) {
            if (criarDialogo()
                    .title("Diretório não encontrado")
                    .text(directory.toString())
                    .instruction("Deseja criar este diretório?").ask()) {
                Files.createDirectories(directory);
                return true;
            }
            return false;
        }
        if (Files.isDirectory(directory)) {
            DirectoryStream<Path> directoryStream = Files.newDirectoryStream(directory);
            if (directoryStream.iterator().hasNext()) {
                if (criarDialogo().title("Diretório não está vazio")
                        .text(directory.toString())
                        .instruction("Deseja limpar este diretório?")
                        .ask()) {
                    Files2.deleteDirectoryRecursive(directory, false, true);
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            criarDialogo()
                    .title("Erro!")
                    .text(directory.toString())
                    .instruction("O caminho não é um diretório, é um arquivo!")
                    .error();
            return false;
        }
    }
    
    private static class RunnableImpl<T> implements Runnable {
        
        private T result;
        private final Producer<T> producer;
        
        public RunnableImpl(Producer<T> producer) {
            this.producer = producer;
        }
        
        @Override
        public void run() {
            this.result = producer.call();
        }
    }
}
