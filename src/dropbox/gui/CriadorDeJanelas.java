/*
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.gui;

import dropbox.server.Server;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JFrame;

/**
 *
 * @author Arthur D'Andréa Alemar
 * @author Daniel Frossard Costa, Douglas Mangili
 * Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco
 */
public class CriadorDeJanelas {
    private final List<JFrame> guis;
    private CriadorDeServidores criadorDeServidores;
    
    private CriadorDeJanelas() {
        this.guis = new LinkedList<>();
    }
    
    public static CriadorDeJanelas getInstance() {
        return CriadorDeJanelasHolder.INSTANCE;
    }
    
    private static class CriadorDeJanelasHolder {

        private static final CriadorDeJanelas INSTANCE = new CriadorDeJanelas();
    }
    
    public ServerGUI criarServerGUI(Server server) {
        final ServerGUI gui = new ServerGUI(server);
        this.guis.add(gui);
        gui.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                guis.remove(gui);
                check();
            }

            
            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e);
                guis.remove(gui);
                check();
            }
        });
        return gui;
    }
    
    public CriadorDeServidores getCriadorDeServidores() {
        if (this.criadorDeServidores == null) {
            this.criadorDeServidores = new CriadorDeServidores();
            this.criadorDeServidores.addWindowListener(new WindowAdapter() {

                @Override
                public void windowClosing(WindowEvent e) {
                    super.windowClosing(e);
                    criadorDeServidores.setVisible(false);
                    check();
                }

                
                @Override
                public void windowClosed(WindowEvent e) {
                    super.windowClosed(e);
                    criadorDeServidores.setVisible(false);
                    check();
                }
            });
        }
        return this.criadorDeServidores;
    }
    
    private void check() {
        if (guis.isEmpty() && (criadorDeServidores == null || !criadorDeServidores.isVisible())) {
            System.exit(0);
        }
    }
}
