/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.gui;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.impl.swing.SwingThreadProxyEventList;
import ca.odell.glazedlists.swing.DefaultEventTableModel;
import ca.odell.glazedlists.util.concurrent.Lock;
import dropbox.server.ConexaoServer;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.SwingUtilities;

/**
 *
 * @author Arthur
 */
class TableModel extends DefaultEventTableModel {
    private PropertyChangeListernetImpl[] propertyChangeListeners;

    public TableModel(EventList<ConexaoServer> source) {
        super(new SwingThreadProxyEventList<>(source), new ConexaoTableFormat());
        if (this.source != null) {
            addPropertyListeners();
        }
    }

    @Override
    protected void handleListChange(ListEvent listChanges) {
        super.handleListChange(listChanges);
        this.addPropertyListeners();
    }

    @Override
    public void dispose() {
        super.dispose();
        if (this.propertyChangeListeners != null) {
            for (PropertyChangeListernetImpl propertyChangeListener : propertyChangeListeners) {
                propertyChangeListener.dessassociate();
            }
            this.propertyChangeListeners = null;
        }
    }

    private void addPropertyListeners() {
        if (this.propertyChangeListeners != null) {
            for (PropertyChangeListernetImpl propertyChangeListener : propertyChangeListeners) {
                propertyChangeListener.dessassociate();
            }
        }
        Lock readLock = source.getReadWriteLock().readLock();
        readLock.lock();
        try {
            this.propertyChangeListeners = new PropertyChangeListernetImpl[source.size()];
            for (int row = 0; row < source.size(); row++) {
                ConexaoServer conexao = (ConexaoServer) source.get(row);
                this.propertyChangeListeners[row] = new PropertyChangeListernetImpl(conexao, row, ConexaoServer.PROP_LASTHEARTBEAT, 2);
                this.propertyChangeListeners[row].associate();
            }
        } finally {
            readLock.unlock();
        }
    }

    private class PropertyChangeListernetImpl implements PropertyChangeListener {
        private final ConexaoServer conexao;
        private final Map<String, Integer> properties;
        private final int row;

        public PropertyChangeListernetImpl(ConexaoServer conexao, int row, String propertyName, int column) {
            this.conexao = conexao;
            this.properties = new TreeMap<>();
            this.properties.put(propertyName, column);
            this.row = row;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            final String propertyName = evt.getPropertyName();
            if (propertyName == null) {
                runInEventDispatchThread(new RowUpdatedRunnable(row, properties.values()));
            } else if (properties.containsKey(propertyName)) {
                runInEventDispatchThread(new CellUpdatedRunnable(row, properties.get(propertyName)));
            }
        }

        public void associate() {
            this.conexao.addPropertyChangeListener(this);
        }

        public void dessassociate() {
            this.conexao.removePropertyChangeListener(this);
        }
    }

    private void runInEventDispatchThread(Runnable runnable) {
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    private class RowUpdatedRunnable implements Runnable {
        private final int row;
        private final Iterable<Integer> columns;

        private RowUpdatedRunnable(int row, Iterable<Integer> columns) {
            this.row = row;
            this.columns = columns;
        }

        @Override
        public void run() {
            for (Integer column : columns) {
                fireTableCellUpdated(row, column);
            }
        }
    }

    private class CellUpdatedRunnable implements Runnable {
        private final int row;
        private final int column;

        public CellUpdatedRunnable(int row, int column) {
            this.row = row;
            this.column = column;
        }

        @Override
        public void run() {
            fireTableCellUpdated(row, column);
        }

    }
}
