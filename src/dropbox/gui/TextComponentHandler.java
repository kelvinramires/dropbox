/*
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 *
 * @author Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili
 * Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco
 */
final class TextComponentHandler extends Handler {
    private static final Logger logger = Logger.getLogger(TextComponentHandler.class.getName());
    private final JTextPane textPane;
    private final SimpleAttributeSet normal;

    TextComponentHandler(JTextPane textPane) {
        this.textPane = textPane;
        this.setFormatter(new SimpleFormatter());
        this.normal = new SimpleAttributeSet();
        StyleConstants.setFontFamily(normal, "SansSerif");
        StyleConstants.setFontSize(normal, 12);
    }

    @Override
    public void publish(LogRecord record) {
        if (!this.isLoggable(record)) {
            return;
        }
        EventQueue.invokeLater(new RunnableImpl(record));
    }

    @Override
    public void flush() {
    }

    @Override
    public void close() throws SecurityException {
    }

    private class RunnableImpl implements Runnable {
        private final LogRecord record;
        private final String formattedRecord;

        private RunnableImpl(LogRecord record) {
            this.record = record;
            this.formattedRecord = getFormatter().format(record);
        }

        @Override
        public void run() {
            StyledDocument document = textPane.getStyledDocument();
            try {
                Level level = record.getLevel();
                int levelValue = level.intValue();
                Color levelColor;
                if (levelValue >= Level.SEVERE.intValue()) {
                    levelColor = Color.RED;
                } else if (levelValue >= Level.WARNING.intValue()) {
                    levelColor = Color.YELLOW;
                } else {
                    levelColor = Color.GREEN;
                }
                SimpleAttributeSet attributes = new SimpleAttributeSet(normal);
                attributes.addAttribute(StyleConstants.Foreground, levelColor);
                document.insertString(document.getEndPosition().getOffset(), formattedRecord, attributes);
                textPane.setCaretPosition(document.getEndPosition().getOffset() - 1);
            } catch (BadLocationException ex) {
                logger.log(Level.SEVERE, "erro ao mostrar o log", ex);
            }
        }
    }
}
