/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.db.dao;

import dropbox.db.model.Arquivo;
import dropbox.errors.ArquivoJaExisteException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import org.postgresql.util.PSQLException;
import org.postgresql.util.ServerErrorMessage;

/**
 *
 * @author Arthur D'Andréa Alemar
 */
public class ArquivoDAO extends BasicDAO {

    private static final ResultSetTransformer<Arquivo> transformer = new ArquivoTranformer();
    private static final ResultSetTransformer<PacoteReplicacao> transformerForReplicacao = new PacoteReplicacaoTransformer();

    ArquivoDAO(DAOFactory dao, Connection connection) {
        super(dao, connection);
    }

    public void create(final Arquivo arquivo) throws SQLException, ArquivoJaExisteException {
        try {
            executeInsideTransaction(new TransactionRunnable() {
                @Override
                public void run() throws SQLException {
                    int id = dao.usuario().getNextIdAndIncrement(arquivo.getUsuarioLogin());
                    arquivo.setId(id);
                    PreparedStatement statement = cacheStatement("create", "INSERT INTO arquivo (caminho, id, usuario_login, lendo, escrevendo) VALUES (?, ?, ?, ?, ?);");
                    statement.setString(1, arquivo.getCaminho());
                    statement.setInt(2, arquivo.getId());
                    statement.setString(3, arquivo.getUsuarioLogin());
                    statement.setBoolean(4, arquivo.isLendo());
                    statement.setBoolean(5, arquivo.isEscrevendo());
                    if (statement.executeUpdate() != 1) {
                        throw new SQLException("erro desconhecido");
                    }
                }
            });
        } catch (PSQLException ex) {
            ServerErrorMessage serverErrorMessage = ex.getServerErrorMessage();
            if ("23505".equalsIgnoreCase(serverErrorMessage.getSQLState()) && "arquivo_caminho_key".equalsIgnoreCase(serverErrorMessage.getConstraint())) {
                throw new ArquivoJaExisteException();
            } else {
                throw ex;
            }
        }
    }

    private static final String associarQuery = "INSERT INTO arquivo_por_servidor (arquivo_id, usuario_login, servidor_id) VALUES (?, ?, ?)";

    public void associar(Arquivo arquivo, int servidorId) throws SQLException {
        PreparedStatement statement = cacheStatement("associar", associarQuery);
        statement.setInt(1, arquivo.getId());
        statement.setString(2, arquivo.getUsuarioLogin());
        statement.setInt(3, servidorId);
        try {
            if (statement.executeUpdate() != 1) {
                throw new SQLException("erro desconhecido");
            }
        } catch (PSQLException ex) {
            ServerErrorMessage serverErrorMessage = ex.getServerErrorMessage();
            if (!"23505".equalsIgnoreCase(serverErrorMessage.getSQLState()) || !"arquivo_por_servidor_pkey".equalsIgnoreCase(serverErrorMessage.getConstraint())) {
                throw ex;
            }
        }
    }

    public Arquivo find(String login, String caminho) throws SQLException {
        PreparedStatement statement = cacheStatement("find", "SELECT * FROM arquivo WHERE usuario_login = ? AND caminho = ? LIMIT 1;");
        statement.setString(1, login);
        statement.setString(2, caminho);
        try (ResultSet results = statement.executeQuery()) {
            return results.next() ? transformer.transformOne(results) : null;
        }
    }

    public boolean estaNoServidor(Arquivo arquivo, int id) throws SQLException {
        PreparedStatement statement = cacheStatement("estaNoServidor", "SELECT 1 FROM arquivo_por_servidor WHERE arquivo_id = ? AND usuario_login = ? AND servidor_id = ? LIMIT 1;");
        statement.setInt(1, arquivo.getId());
        statement.setString(2, arquivo.getUsuarioLogin());
        statement.setInt(3, id);
        try (ResultSet result = statement.executeQuery()) {
            return result.next() && result.getInt(1) == 1;
        }
    }

    public int algumServidorCom(Arquivo arquivo) throws SQLException {
        PreparedStatement statement = cacheStatement("algumServidorCom", "SELECT servidor_id FROM arquivo_por_servidor WHERE arquivo_id = ? AND usuario_login = ? ORDER BY random() LIMIT 1;");
        statement.setInt(1, arquivo.getId());
        statement.setString(2, arquivo.getUsuarioLogin());
        try (ResultSet result = statement.executeQuery()) {
            if (result.next()) {
                return result.getInt(1);
            } else {
                throw new SQLException("não encontrado!");
            }
        }
    }

    private static final String countQuery = "SELECT COUNT(*) FROM arquivo;";

    public int count() throws SQLException {
        PreparedStatement statement = this.cacheStatement("count", countQuery);
        try (ResultSet result = statement.executeQuery()) {
            if (!result.next()) {
                throw new SQLException("Erro desconhecido");
            }
            return result.getInt(1);
        }
    }

    private static final String deleteAllQuery = "DELETE FROM arquivo;";

    public void deleteAll() throws SQLException {
        PreparedStatement statement = this.cacheStatement("deleteAll", deleteAllQuery);
        statement.executeUpdate();
    }

    private static final String allFromUsuarioQuery = "SELECT * FROM arquivo WHERE usuario_login = ?;";

    public List<Arquivo> allFromUsuario(String usuario) throws SQLException {
        PreparedStatement statement = this.cacheStatement("allFromUsuario", allFromUsuarioQuery);
        statement.setString(1, usuario);
        try (ResultSet results = statement.executeQuery()) {
            return transformer.transformList(results);
        }
    }

    private static final String replicacaoQuery = "SELECT mp.id, mp.usuario_login, mp.count, array_agg(s.id) as servidores FROM servidor s, \n"
            + "	(SELECT a.*, COUNT(aps.arquivo_id) as count\n"
            + "	FROM arquivo a, arquivo_por_servidor aps\n"
            + "	WHERE a.id = aps.arquivo_id AND a.usuario_login = aps.usuario_login\n"
            + "	AND EXISTS (SELECT 1 FROM arquivo_por_servidor aps2 WHERE aps2.arquivo_id = a.id AND aps2.usuario_login = a.usuario_login AND aps2.servidor_id = ?)\n"
            + "	GROUP BY a.id, a.usuario_login\n"
            + "	HAVING COUNT(aps.arquivo_id) < ?) mp\n"
            + "WHERE NOT EXISTS (SELECT 1 FROM arquivo_por_servidor aps3 WHERE aps3.servidor_id = s.id AND aps3.arquivo_id = mp.id AND aps3.usuario_login = mp.usuario_login)\n"
            + "group by mp.id, mp.usuario_login, mp.count";

    /**
     * Retorna os arquivos que precisam de replicação e que estejam associados
     * com o servidor passado.
     *
     * @param serverId servidor
     * @param replicacaoMinima replicação mínima
     * @return os arquivos que precisam de replicação e que estejam associados
     * com o servidor passado.
     * @throws SQLException quando um erro SQL inesperado acontece
     */
    public List<PacoteReplicacao> replicacao(int serverId, int replicacaoMinima) throws SQLException {
        PreparedStatement statement = this.cacheStatement("replicacao", replicacaoQuery);
        statement.setInt(1, serverId);
        statement.setInt(2, replicacaoMinima + 1);
        try (ResultSet resultSet = statement.executeQuery()) {
            return transformerForReplicacao.transformList(resultSet);
        }
    }

    private static final String deleteQuery = "DELETE FROM arquivo WHERE id = ? AND usuario_login = ?";

    public void delete(Arquivo arquivo) throws SQLException {
        PreparedStatement statement = this.cacheStatement("delete", deleteQuery);
        statement.setInt(1, arquivo.getId());
        statement.setString(2, arquivo.getUsuarioLogin());
        statement.executeUpdate();
    }

    private static class ArquivoTranformer extends ResultSetTransformer<Arquivo> {

        @Override
        public Arquivo transformOne(ResultSet resultSet) throws SQLException {
            Arquivo arquivo = new Arquivo();
            arquivo.setId(resultSet.getInt("id"));
            arquivo.setCaminho(resultSet.getString("caminho"));
            arquivo.setUsuarioLogin(resultSet.getString("usuario_login"));
            arquivo.setLendo(resultSet.getBoolean("lendo"));
            arquivo.setEscrevendo(resultSet.getBoolean("escrevendo"));
            return arquivo;
        }
    }

    public static class PacoteReplicacao {

        private Arquivo arquivo;
        private int count;
        private List<Integer> servidores;

        public Arquivo getArquivo() {
            return arquivo;
        }

        public void setArquivo(Arquivo arquivo) {
            this.arquivo = arquivo;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public List<Integer> getServidores() {
            return servidores;
        }

        public void setServidores(List<Integer> servidores) {
            this.servidores = servidores;
        }
    }

    private static class PacoteReplicacaoTransformer extends ResultSetTransformer<PacoteReplicacao> {

        @Override
        public PacoteReplicacao transformOne(ResultSet resultSet) throws SQLException {
            PacoteReplicacao pacote = new PacoteReplicacao();
            Arquivo arquivo = new Arquivo();
            arquivo.setId(resultSet.getInt("id"));
            arquivo.setUsuarioLogin(resultSet.getString("usuario_login"));
            pacote.setArquivo(arquivo);
            pacote.setCount(resultSet.getInt("count"));
            Array array = resultSet.getArray("servidores");
            pacote.setServidores(Arrays.asList((Integer[]) array.getArray()));
            return pacote;
        }
    }

}
