/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.db.dao;

import dropbox.db.model.Usuario;
import dropbox.errors.UsuarioJaExisteException;
import dropbox.errors.UsuarioNaoExisteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.postgresql.util.PSQLException;
import org.postgresql.util.ServerErrorMessage;

/**
 *
 * @author Arthur
 */
public class UsuarioDAO extends BasicDAO {
    private static final ResultSetTransformer<Usuario> transformer = new ResultSetTransformer<Usuario>() {
        @Override
        public Usuario transformOne(ResultSet resultSet) throws SQLException {
            Usuario usuario = new Usuario();
            usuario.setLogin(resultSet.getString("login"));
            usuario.setSenha(resultSet.getString("senha"));
            return usuario;
        }
    };

    UsuarioDAO(DAOFactory dao, Connection connection) {
        super(dao, connection);
    }

    public void create(Usuario usuario) throws SQLException, UsuarioJaExisteException {
        PreparedStatement statement = this.cacheStatement("create", "INSERT INTO usuario (login, senha) VALUES (?, ?);");
        statement.setString(1, usuario.getLogin());
        statement.setString(2, usuario.getSenha());
        try {
            if (statement.executeUpdate() != 1) {
                throw new SQLException("erro desconhecido");
            }
        } catch(PSQLException ex) {
            ServerErrorMessage serverErrorMessage = ex.getServerErrorMessage();
            if ("23505".equalsIgnoreCase(serverErrorMessage.getSQLState()) && "usuario_pkey".equalsIgnoreCase(serverErrorMessage.getConstraint())) {
                throw new UsuarioJaExisteException();
            } else {
                throw ex;
            }
        }
    }
    
    public int getNextIdAndIncrement(final String usuarioLogin) throws SQLException {
        return this.executeInsideTransaction(new TransactionProducer<Integer>() {
            @Override
            public Integer run() throws SQLException {
                int id = getNextId(usuarioLogin);
                incrementNextId(usuarioLogin);
                return id;
            }
        });
    }

    private void incrementNextId(String usuarioLogin) throws SQLException {
        PreparedStatement statement = this.cacheStatement("inc_next_id", "UPDATE usuario SET next_id = next_id + 1 WHERE login = ?;");
        statement.setString(1, usuarioLogin);
        if (statement.executeUpdate() != 1) {
            throw new SQLException("erro desconhecido");
        }
    }

    private int getNextId(String usuarioLogin) throws SQLException {
        PreparedStatement statement = this.cacheStatement("get_next_id", "SELECT next_id FROM usuario WHERE login = ?;");
        statement.setString(1, usuarioLogin);
        try (ResultSet result = statement.executeQuery()) {
            if (!result.next()) {
                throw new SQLException("erro desconhecido");
            }
            return result.getInt("next_id");
        }
    }

    private static final String countQuery = "SELECT COUNT(*) FROM usuario;";
    public int count() throws SQLException {
        PreparedStatement statement = this.cacheStatement("count", countQuery);
        try (ResultSet result = statement.executeQuery()) {
            if (!result.next()) {
                throw new SQLException("Erro desconhecido");
            }
            return result.getInt(1);
        }
    }

    private static final String deleteAllQuery = "DELETE FROM usuario;";
    public void deleteAll() throws SQLException {
        PreparedStatement statement = this.cacheStatement("deleteAll", deleteAllQuery);
        statement.executeUpdate();
    }

    private static final String findByLoginQuery = "SELECT * FROM usuario WHERE login = ?;";
    public Usuario findByLogin(String login) throws SQLException, UsuarioNaoExisteException {
        PreparedStatement statement = this.cacheStatement("findByLogin", findByLoginQuery);
        statement.setString(1, login);
        try (ResultSet result = statement.executeQuery()) {
            if (result.next()) {
                return transformer.transformOne(result);
            } else {
                throw new UsuarioNaoExisteException();
            }
        }
    }
}
