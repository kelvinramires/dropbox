/*
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili
 * Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco
 */
public abstract class ResultSetTransformer<T> {
    public abstract T transformOne(ResultSet resultSet) throws SQLException;
    public final List<T> transformList(ResultSet resultSet) throws SQLException {
        List<T> list = new LinkedList<>();
        while (resultSet.next()) {
            list.add(transformOne(resultSet));
        }
        return list;
    }
}
