/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.db.dao;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Arthur D'Andréa Alemar
 */
public class DAOFactory implements AutoCloseable {
    private final Connection connection;
    private UsuarioDAO usuario;
    private ArquivoDAO arquivo;
    private ServidorDAO servidor;
    
    public DAOFactory(Connection connection) {
        this.connection = connection;
    }
    
    public ArquivoDAO arquivo() {
        if (this.arquivo == null) {
            this.arquivo = new ArquivoDAO(this, connection);
        }
        return this.arquivo;
    }
    
    public ServidorDAO servidor() {
        if (this.servidor == null) {
            this.servidor = new ServidorDAO(this, connection);
        }
        return this.servidor;
    }

    public UsuarioDAO usuario() {
        if (this.usuario == null) {
            this.usuario = new UsuarioDAO(this, connection);
        }
        return this.usuario;
    }

    @Override
    public void close() throws SQLException {
        if (this.servidor != null) {
            this.servidor.close();
            this.servidor = null;
        }
        if (this.usuario != null) {
            this.usuario.close();
            this.usuario = null;
        }
        if (this.arquivo != null) {
            this.arquivo.close();
            this.arquivo = null;
        }
        this.connection.close();
    }
}
