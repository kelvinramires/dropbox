/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.db.dao;

import dropbox.db.model.Servidor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Arthur D'Andréa Alemar
 */
public class ServidorDAO extends BasicDAO {
    private static final ResultSetTransformer<Servidor> transformer = new ResultSetTransformer<Servidor>() {
        @Override
        public Servidor transformOne(ResultSet resultSet) throws SQLException {
            Servidor servidor = new Servidor();
            servidor.setId(resultSet.getInt("id"));
            servidor.setPorta(resultSet.getInt("porta"));
            servidor.setHost(resultSet.getString("host"));
            return servidor;
        }
    };

    ServidorDAO(DAOFactory dao, Connection connection) {
        super(dao, connection);
    }

    private static final String createQuery = "INSERT INTO servidor (id, host, porta) VALUES (?, ?, ?);";
    public void create(Servidor servidor) throws SQLException {
        PreparedStatement statement = this.cacheStatement("create", createQuery);
        statement.setInt(1, servidor.getId());
        statement.setString(2, servidor.getHost());
        statement.setInt(3, servidor.getPorta());
        if (statement.executeUpdate() != 1) {
            throw new SQLException("Erro desconhecido");
        }
    }

    private static final String findByIdQuery = "SELECT * FROM servidor WHERE id = ?;";
    public Servidor findById(int id) throws SQLException {
        PreparedStatement statement = this.cacheStatement("findById", findByIdQuery);
        statement.setInt(1, id);
        try (ResultSet result = statement.executeQuery()) {
            if (result.next()) {
                return transformer.transformOne(result);
            } else {
                return null;
            }
        }
    }

    private static final String allQuery = "SELECT * FROM servidor;";
    public List<Servidor> all() throws SQLException {
        PreparedStatement statement = this.cacheStatement("all", allQuery);
        try (ResultSet result = statement.executeQuery()) {
            return transformer.transformList(result);
        }
    }

    private static final String countQuery = "SELECT COUNT(*) FROM servidor;";
    public int count() throws SQLException {
        PreparedStatement statement = this.cacheStatement("count", countQuery);
        try (ResultSet result = statement.executeQuery()) {
            if (!result.next()) {
                throw new SQLException("Erro desconhecido");
            }
            return result.getInt(1);
        }
    }

    private static final String deleteAllQuery = "DELETE FROM servidor;";
    public void deleteAll() throws SQLException {
        PreparedStatement statement = this.cacheStatement("deleteAll", deleteAllQuery);
        statement.executeUpdate();
    }

    private static final String deleteQuery = "DELETE FROM servidor WHERE id = ?;";
    public void delete(int id) throws SQLException {
        PreparedStatement statement = this.cacheStatement("delete", deleteQuery);
        statement.setInt(1, id);
        statement.executeUpdate();
    }
}
