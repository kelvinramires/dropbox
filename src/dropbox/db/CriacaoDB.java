/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.db;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.postgresql.core.Utils;

/**
 *
 * @author Arthur D'Andréa Alemar
 * @author Kelvin Ramires Capobianco
 */
public class CriacaoDB {
    private static final String tabelaUsuario = "login VARCHAR(255) PRIMARY KEY,"
            + " senha VARCHAR(255) NOT NULL,"
            + " next_id INTEGER NOT NULL DEFAULT 1";
    private static final String tabelaServidor = "id INTEGER PRIMARY KEY,"
            + " host VARCHAR(255) NOT NULL,"
            + " porta INTEGER";
    private static final String tabelaArquivo = "id INTEGER NOT NULL,"
            + " caminho VARCHAR(255) NOT NULL UNIQUE,"
            + " usuario_login VARCHAR(255) NOT NULL,"
            + " lendo BOOLEAN NOT NULL,"
            + " escrevendo BOOLEAN NOT NULL,"
            + " PRIMARY KEY (usuario_login, id), "
            + " FOREIGN KEY (usuario_login) REFERENCES usuario(login)"
            + " ON DELETE RESTRICT"
            + " ON UPDATE RESTRICT";
    private static final String tabelaArquivoPorServidor = "arquivo_id INTEGER NOT NULL, "
            + "usuario_login VARCHAR(255) NOT NULL, "
            + "servidor_id INTEGER NOT NULL, "
            + "PRIMARY KEY (arquivo_id, usuario_login, servidor_id), "
            + "FOREIGN KEY (arquivo_id, usuario_login) REFERENCES arquivo(id, usuario_login) "
            + "ON DELETE CASCADE "
            + "ON UPDATE RESTRICT, "
            + "FOREIGN KEY (servidor_id) REFERENCES servidor(id) "
            + "ON DELETE CASCADE "
            + "ON UPDATE RESTRICT";
    private final Connection connection;

    public CriacaoDB(Connection connection) {
        this.connection = connection;
    }

    public void criarTabelas() throws SQLException {
        if (this.connection.getAutoCommit()) {
            this.connection.setAutoCommit(false);
            try {
                this.criarTabelas2();
                this.connection.commit();
            } catch(Throwable ex) {
                this.connection.rollback();
                throw ex;
            } finally {
                this.connection.setAutoCommit(true);
            }
        } else {
            this.criarTabelas2();
        }
    }

    private void criarTabelas2() throws SQLException {
        createTable("usuario", tabelaUsuario);
        createTable("servidor", tabelaServidor);
        createTable("arquivo", tabelaArquivo);
        createTable("arquivo_por_servidor", tabelaArquivoPorServidor);
    }
    
    private void createTable(String tableName, String columns) throws SQLException {
        StringBuffer builder = new StringBuffer();
        builder.append("CREATE TABLE ");
        Utils.appendEscapedIdentifier(builder, tableName);
        builder.append('(');
        builder.append(columns);
        builder.append(");");
        this.executeSQL(builder);
    }

    private void executeSQL(String sql) throws SQLException {
        try (Statement statement = this.connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }
    
    private void executeSQL(StringBuffer builder) throws SQLException {
        this.executeSQL(builder.toString());
    }

    public void criarDatabase(String dbName) throws SQLException {
        StringBuffer builder = new StringBuffer();
        builder.append("CREATE DATABASE ");
        Utils.appendEscapedIdentifier(builder, dbName);
        builder.append(";");
        this.executeSQL(builder);
    }

    public void dropDatabase(String dbName) throws SQLException {
        StringBuffer builder = new StringBuffer();
        builder.append("DROP DATABASE ");
        Utils.appendEscapedIdentifier(builder, dbName);
        builder.append(";");
        this.executeSQL(builder);
    }
    
    public boolean hasDatabase(String dbName) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("SELECT 1 FROM pg_database WHERE datname = ?;")) {
            statement.setString(1, dbName);
            try (ResultSet result = statement.executeQuery()) {
                return result.next() && result.getInt(1) == 1;
            }
        }
    }
}