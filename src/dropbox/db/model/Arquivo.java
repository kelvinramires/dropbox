/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.db.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 *
 * @author Arthur
 */
public class Arquivo implements Serializable {
    private String caminho;
    private int id;
    private boolean lendo;
    private boolean escrevendo;
    private String usuarioLogin;
    private int count;

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isLendo() {
        return lendo;
    }

    public void setLendo(boolean lendo) {
        this.lendo = lendo;
    }

    public boolean isEscrevendo() {
        return escrevendo;
    }

    public void setEscrevendo(boolean escrevendo) {
        this.escrevendo = escrevendo;
    }

    public String getUsuarioLogin() {
        return usuarioLogin;
    }

    public void setUsuarioLogin(String usuarioLogin) {
        this.usuarioLogin = usuarioLogin;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public OutputStream abrirParaEscrita(Path diretorio) throws IOException {
        Path path = getPath(diretorio);
        Files.createDirectories(path.getParent());
        return Files.newOutputStream(path, StandardOpenOption.CREATE);
    }

    public InputStream abrirParaLeitura(Path diretorio) throws IOException {
        return Files.newInputStream(getPath(diretorio));
    }

    public Path getPath(Path diretorio) {
        return diretorio.resolve("dados").resolve(usuarioLogin).resolve(Integer.toString(id));
    }

    public boolean existe(Path diretorio) {
        return Files.exists(this.getPath(diretorio));
    }
}
