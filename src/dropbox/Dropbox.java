/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox;

import com.ezware.dialog.task.CommandLink;
import com.ezware.dialog.task.TaskDialogs;
import dropbox.gui.ClienteGUI;
import dropbox.gui.CriadorDeJanelas;
import java.awt.EventQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Kelvin
 */
public class Dropbox implements Runnable {
    private static final Logger logger = Logger.getLogger(Dropbox.class.getName());

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        EventQueue.invokeLater(new Dropbox());
    }

    @Override
    public void run() {
        int choice = TaskDialogs.build()
                .title("Iniciando Dropbox")
                .instruction("Escolha o modo de início: servidor ou cliente.")
                .text("Por<br /><i>Arthur D'Andréa Alemar<br />Daniel Frossard Costa<br />Douglas Mangili Crespo<br />Emmanuel da Costa Galo<br />Kelvin Ramires Capobianco</i>")
                .choice(0, new CommandLink("Servidor", "Inicia como um servidor que armazena<br />arquivos distriuídos"),
                        new CommandLink("Cliente", "Inicia como um cliente que usa servidores<br />distribuídos para o armazenamento de arquivos"));
        JFrame window;
        switch (choice) {
            case 0:
                window = CriadorDeJanelas.getInstance().getCriadorDeServidores();
                break;
            case 1:
                window = new ClienteGUI();
                break;
            default:
                System.exit(0);
                return;
        }
        window.setVisible(true);
    }
    
}
