/*
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.util;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili
 * Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco
 */
public abstract class HeartBeat {

    private static final Logger logger = Logger.getLogger(HeartBeat.class.getName());

    private final ScheduledExecutorService threadPool;
    private final WaitHeartBeatRunnable waitHeartBeatRunnable;
    private final SendHeartBeatRunnable sendHeartBeatRunnable;
    private final long sendDelay;
    private final long waitDelay;
    private final ReentrantLock lock;
    private ScheduledFuture<?> sendHeartBeatFuture;
    private ScheduledFuture<?> waitHeartBeatFuture;

    public HeartBeat(ScheduledExecutorService threadPool, long sendDelay, long waitDelay) {
        waitHeartBeatRunnable = new WaitHeartBeatRunnable();
        sendHeartBeatRunnable = new SendHeartBeatRunnable();
        this.threadPool = threadPool;
        this.sendDelay = sendDelay;
        this.waitDelay = waitDelay;
        this.lock = new ReentrantLock();
    }

    protected abstract void enviarHeartBeat() throws Exception;

    public final void schedule() {
        this.lock.lock();
        try {
            cancelSendHeartBeat();
            cancelWaitHeartBeat();
            this.sendHeartBeatFuture = this.threadPool.scheduleWithFixedDelay(sendHeartBeatRunnable, this.sendDelay, this.sendDelay, TimeUnit.MILLISECONDS);
            this.waitHeartBeatFuture = this.threadPool.schedule(waitHeartBeatRunnable, this.waitDelay, TimeUnit.MILLISECONDS);
        } finally {
            this.lock.unlock();
        }
    }

    public void hearbeatCallback(boolean sucesso) {
        this.lock.lock();
        try {
            cancelWaitHeartBeat();
            if (sucesso) {
                this.waitHeartBeatFuture = this.threadPool.schedule(waitHeartBeatRunnable, this.waitDelay, TimeUnit.MILLISECONDS);
            } else {
                cancelSendHeartBeat();
            }
        } finally {
            this.lock.unlock();
        }
    }

    private void cancelSendHeartBeat() {
        if (this.sendHeartBeatFuture == null) {
            return;
        }
        this.sendHeartBeatFuture.cancel(false);
        this.sendHeartBeatFuture = null;
    }

    private void cancelWaitHeartBeat() {
        if (this.waitHeartBeatFuture == null) {
            return;
        }
        this.waitHeartBeatFuture.cancel(false);
        this.waitHeartBeatFuture = null;
    }

    private class WaitHeartBeatRunnable implements Runnable {

        @Override
        public void run() {
            hearbeatCallback(false);
        }
    }

    private class SendHeartBeatRunnable implements Runnable {

        @Override
        public void run() {
            boolean sucesso;
            try {
                enviarHeartBeat();
                sucesso = true;
            } catch (Throwable ex) {
                logger.log(Level.SEVERE, "erro ao enviar HeartBeat", ex);
                sucesso = false;
            }
            hearbeatCallback(sucesso);
        }
    }
}
