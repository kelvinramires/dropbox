/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.util;

import com.sun.jna.platform.FileUtils;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 *
 * @author Arthur
 */
public final class Files2 {

    private static final DeleteVisitor deleteVisitor = new DeleteVisitor();

    private Files2() {
    }

    public static void deleteDirectoryRecursive(Path directory) throws IOException {
        Files.walkFileTree(directory, deleteVisitor);
        Files.delete(directory);
    }

    public static void deleteDirectoryRecursive(Path directory, boolean deleteDir) throws IOException {
        deleteDirectoryRecursive(directory, deleteDir, false);
    }

    /**
     * Deleta um diretório recursivamente
     * 
     * @param directory diretório a ser deletado
     * @param deleteDir verdadeiro para deletar também o próprio diretório
     * @param useTrash verdadeiro para usar a lixeira do sistema se houver
     * @throws IOException quando acontece um erro de entrada e saída
     */
    public static void deleteDirectoryRecursive(Path directory, boolean deleteDir, boolean useTrash) throws IOException {
        if (!Files.exists(directory)) {
            return;
        }
        FileUtils fileUtils = FileUtils.getInstance();
        if (useTrash && fileUtils.hasTrash()) {
            fileUtils.moveToTrash(new File[] { directory.toFile() });
        } else {
            Files.walkFileTree(directory, deleteVisitor);
            if (deleteDir) {
                Files.delete(directory);
            }
        }
    }

    private static class DeleteVisitor extends SimpleFileVisitor<Path> {

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            Files.delete(file);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            Files.delete(dir);
            return FileVisitResult.CONTINUE;
        }
    }
}
