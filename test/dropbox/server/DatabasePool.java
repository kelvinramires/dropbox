/*
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.server;

import dropbox.db.ConexaoInfo;
import dropbox.db.CriacaoDB;
import dropbox.db.dao.DAOFactory;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Stack;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili
 * Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco
 */
public class DatabasePool {
    private final ReentrantLock lock;
    private final ArrayDeque<ConexaoInfo> freeDatabases;
    private final ConexaoInfo infoADM;
    private int i;

    public DatabasePool(ConexaoInfo infoADM) {
        this.lock = new ReentrantLock();
        this.freeDatabases = new ArrayDeque<>();
        this.infoADM = infoADM;
        this.i = 0;
    }
    
    public ConexaoInfo getDatabase() throws SQLException {
        this.lock.lock();
        try {
            if (this.freeDatabases.size() > 0) {
                return this.freeDatabases.pop();
            } else {
                String dbName;
                try (Connection admConnection = infoADM.createConnection()) {
                    CriacaoDB criacaoADM = new CriacaoDB(admConnection);
                    do {
                        dbName = "dropbox_test_" + (++i);
                    } while (criacaoADM.hasDatabase(dbName) && i < 10);
                    if (i == 10) {
                        throw new RuntimeException("Muitos databases criados");
                    }
                    criacaoADM.criarDatabase(dbName);
                }
                ConexaoInfo info = infoADM.clone();
                info.setDatabase(dbName);
                try (Connection connection = info.createConnection()) {
                    new CriacaoDB(connection).criarTabelas();
                }
                return info;
            }
        } finally {
            this.lock.unlock();
        }
    }
    
    public void releaseDatabase(ConexaoInfo info) throws SQLException {
        this.lock.lock();
        try {
            try (DAOFactory dao = new DAOFactory(info.createConnection())) {
                dao.servidor().deleteAll();
                dao.arquivo().deleteAll();
                dao.usuario().deleteAll();
            }
            this.freeDatabases.push(info);
        } finally {
            this.lock.unlock();
        }
    }
    
    public void dispose() throws SQLException {
        this.lock.lock();
        try {
            try (Connection admConnection = infoADM.createConnection()) {
                CriacaoDB criacaoADM = new CriacaoDB(admConnection);
                while (!freeDatabases.isEmpty()) {
                    ConexaoInfo database = freeDatabases.pop();
                    criacaoADM.dropDatabase(database.getDatabase());
                }
            }
        } finally {
            this.lock.unlock();
        }
    }
}
