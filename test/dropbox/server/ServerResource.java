/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.server;

import dropbox.db.ConexaoInfo;
import dropbox.db.ConnectionPool;
import dropbox.db.CriacaoDB;
import dropbox.util.Files2;
import java.nio.file.Path;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Kelvin
 */
public class ServerResource implements AutoCloseable {
    private final Path directory;
    private final ConexaoInfo infoAdm;
    private final String dbName;
    private Server server;
    private final int id;
    private final ConexaoInfo info;

    public ServerResource(int id, Path directory, ConexaoInfo conexaoInfo, String dbName) {
        this.directory = directory.resolve("server" + id);
        this.infoAdm = conexaoInfo;
        this.info = conexaoInfo.clone();
        this.dbName = dbName;
        this.server = null;
        this.id = id;
    }
    
    public Server getServer() throws ClassNotFoundException, RemoteException, SQLException {
        if (this.server != null) {
            this.server = new Server(id, "localhost", 2000 + id, this.directory, new ConnectionPool(info));
        }
        return this.server;
    }
    
    @Override
    public void close() throws Exception {
        if (this.server != null) {
            this.server.stop();
        }
        try (Connection connection = this.infoAdm.createConnection()) {
            new CriacaoDB(connection).dropDatabase(dbName);
        }
        Files2.deleteDirectoryRecursive(this.directory);
    }    
}
