/* 
 * The MIT License
 *
 * Copyright 2014 Arthur D'Andréa Alemar, Daniel Frossard Costa, Douglas Mangili Crespo, Emmanuel da Costa Galo, Kelvin Ramires Capobianco.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dropbox.server;

import dropbox.db.ConexaoInfo;
import dropbox.db.ConnectionPool;
import dropbox.errors.ArquivoJaExisteException;
import dropbox.errors.ArquivoNaoExisteException;
import dropbox.errors.UsuarioJaExisteException;
import dropbox.util.Files2;
import dropbox.util.RMIRegistryFactory;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Kelvin
 */
public class ServerTest {
    private static final String senhaDB = "";
    private static final String usuarioDB = "Arthur";
    private static DatabasePool databasePool;

    private static Path tempdir;
    private Random random;
    private Path[] pastas;
    private Server[] servers;
    private ConexaoInfo[] databases;
    private int qtd;
    
    @BeforeClass
    public static void setUpClass() throws IOException, SQLException {
        ConexaoInfo infoADM = new ConexaoInfo();
        infoADM.setDatabase("postgres");
        infoADM.setUsername(usuarioDB);
        infoADM.setPassword(senhaDB);
        databasePool = new DatabasePool(infoADM);
        for (int i = 0; i < 3; i++) {
            ConexaoInfo database = databasePool.getDatabase();
            databasePool.releaseDatabase(database);
        }
        tempdir = FileSystems.getDefault().getPath("testes");
        if (Files.notExists(tempdir)) {
            Files.createDirectory(tempdir);
        }
    }
    
    @AfterClass
    public static void tearDownClass() throws SQLException {
        databasePool.dispose();
    }
        
    @Before
    public void setUp() throws IOException, SQLException {
        this.random = new Random();
        this.qtd = 3;
        this.pastas = new Path[qtd];
        this.servers = new Server[qtd];
        this.databases = new ConexaoInfo[qtd];
        for (int i = 0; i < qtd; i++) {
            this.pastas[i] = tempdir.resolve("server" + (i + 1));
            if (Files.notExists(this.pastas[i])) {
                Files.createDirectory(this.pastas[i]);
            } else {
                Files2.deleteDirectoryRecursive(this.pastas[i], false);
            }
            this.databases[i] = databasePool.getDatabase();
            int porta = 2000 + i;
            this.servers[i] = new Server(i+1, "localhost", porta, this.pastas[i], new ConnectionPool(this.databases[i]), RMIRegistryFactory.getInstance().getRegistry(porta));
        }
    }

    @After
    public void tearDown() throws IOException, RemoteException, NotBoundException, SQLException {
        for (Server server : this.servers) {
            if (server != null) {
                server.stopParaTeste();
            }
        }
        for (ConexaoInfo database : this.databases) {
            if (database != null) {
                databasePool.releaseDatabase(database);
            }
        }
    }

    @Test
    public void testUm() throws Exception {
        this.servers[0].adicionarUsuario("arthur", "1234");
        byte[] dadosEscritos = new byte[this.random.nextInt(1023) + 1];
        this.random.nextBytes(dadosEscritos);
        this.servers[0].adicionarArquivo("arthur", "arquivo1.txt", dadosEscritos);
        byte[] dadosLidos = this.servers[0].lerArquivo("arthur", "arquivo1.txt");
        assertArrayEquals(dadosEscritos, dadosLidos);
    }

    @Test
    public void testDois() throws Exception {
        for (int i = 0; i < qtd; i++) {
            this.servers[i].setReplicacaoMinima(1);
        }
        this.servers[0].conectar(this.servers[1].getHost(), this.servers[1].getPorta());
        this.servers[0].adicionarUsuario("arthur", "1234");
        byte[] dadosEscritos = new byte[this.random.nextInt(1023) + 1];
        this.random.nextBytes(dadosEscritos);
        this.servers[0].adicionarArquivo("arthur", "arquivo1.txt", dadosEscritos);
        assertTrue(this.servers[0].temOArquivoLocal("arthur", "arquivo1.txt"));
        assertTrue(this.servers[1].temOArquivoLocal("arthur", "arquivo1.txt"));
        byte[] dadosLidos = this.servers[1].lerArquivo("arthur", "arquivo1.txt");
        assertArrayEquals(dadosEscritos, dadosLidos);
    }
    
    @Test
    public void testTres() throws Exception {
        for (int i = 0; i < qtd; i++) {
            this.servers[i].setReplicacaoMinima(0);
        }
        this.servers[0].conectar(this.servers[1].getHost(), this.servers[1].getPorta());
        this.servers[0].adicionarUsuario("arthur", "1234");
        byte[] dadosEscritos = new byte[this.random.nextInt(1023) + 1];
        this.random.nextBytes(dadosEscritos);
        this.servers[0].adicionarArquivo("arthur", "arquivo1.txt", dadosEscritos);
        assertTrue(this.servers[0].temOArquivoLocal("arthur", "arquivo1.txt"));
        assertFalse(this.servers[1].temOArquivoLocal("arthur", "arquivo1.txt"));
        byte[] dadosLidos = this.servers[1].lerArquivo("arthur", "arquivo1.txt");
        assertArrayEquals(dadosEscritos, dadosLidos);
    }
    
    @Test
    public void testQuatro() throws Exception {
        for (int i = 0; i < qtd; i++) {
            this.servers[i].setReplicacaoMinima(1);
        }
        this.servers[0].conectar(this.servers[1].getHost(), this.servers[1].getPorta());
        this.servers[0].conectar(this.servers[2].getHost(), this.servers[2].getPorta());
        this.servers[0].adicionarUsuario("arthur", "1234");
        for (int i = 0; i < 10; i++) {
            byte[] dadosEscritos = new byte[this.random.nextInt(1023) + 1];
            this.random.nextBytes(dadosEscritos);
            String nomedoarquivo = "arquivo" + i + ".txt";
            this.servers[0].adicionarArquivo("arthur", nomedoarquivo, dadosEscritos);
            assertTrue(this.servers[0].temOArquivoLocal("arthur", nomedoarquivo));
            byte[] dadosLidos = this.servers[1].lerArquivo("arthur", nomedoarquivo);
            assertArrayEquals(dadosEscritos, dadosLidos);
            
            int count = 0;
            for (int j = 0; j < 3; j++) {
                if (this.servers[j].temOArquivoLocal("arthur", nomedoarquivo)) {
                    count++;
                }
            }
            assertEquals(2, count);
        }
        this.servers[2].stopParaTeste();
        Thread.sleep(1000);
        for (int i = 0; i < 10; i++) {
            final String nomedoarquivo = "arquivo" + i + ".txt";
            int count = 0;
            for (int j = 0; j < 2; j++) {
                if (this.servers[j].temOArquivoLocal("arthur", nomedoarquivo)) {
                    count++;
                }
            }
            assertEquals(2, count);
        }
    }
    
    @Test
    public void testCinco() throws Exception {
        for (int i = 0; i < qtd; i++) {
            this.servers[i].setReplicacaoMinima(1);
        }
        this.servers[0].conectar(this.servers[1].getHost(), this.servers[1].getPorta());
        this.servers[0].adicionarUsuario("arthur", "1234");
        byte[] dadosEscritos = new byte[]{1, 2, 3};
        this.servers[0].adicionarArquivo("arthur", "arquivo1.txt", dadosEscritos);
        assertTrue(this.servers[0].temOArquivoLocal("arthur", "arquivo1.txt"));
        assertTrue(this.servers[1].temOArquivoLocal("arthur", "arquivo1.txt"));
        byte[] dadosLidos = this.servers[1].lerArquivo("arthur", "arquivo1.txt");
        assertArrayEquals(dadosEscritos, dadosLidos);
        
        this.servers[1].removerArquivo("arthur", "arquivo1.txt");
        assertFalse(this.servers[0].temOArquivo("arthur", "arquivo1.txt"));
        assertFalse(this.servers[1].temOArquivo("arthur", "arquivo1.txt"));
    }
    
    @Test(expected = UsuarioJaExisteException.class)
    public void testAdicionarUsuario() throws RemoteException, NotBoundException, SQLException, UsuarioJaExisteException {
        this.servers[0].conectar(this.servers[1].getHost(), this.servers[1].getPorta());
        this.servers[0].adicionarUsuario("bola", "bola");
        this.servers[1].adicionarUsuario("bola", "bola");
    }
    
    @Test(expected = ArquivoJaExisteException.class)
    public void testAdicionarArquivo() throws RemoteException, NotBoundException, SQLException, UsuarioJaExisteException, IOException, ArquivoJaExisteException {
        this.servers[0].conectar(this.servers[1].getHost(), this.servers[1].getPorta());
        this.servers[1].adicionarUsuario("bola", "bola");
        byte[] dados = new byte[this.random.nextInt(1023) + 1];
        this.random.nextBytes(dados);
        this.servers[0].adicionarArquivo("bola", "pasta/arquivo.exe", dados);
        this.servers[0].adicionarArquivo("bola", "pasta/arquivo.exe", dados);
    }

    @Test(expected = ArquivoNaoExisteException.class)
    public void testRemoverArquivo()
            throws RemoteException, NotBoundException, SQLException,
            UsuarioJaExisteException, IOException, ArquivoNaoExisteException {
        this.servers[0].conectar(this.servers[1].getHost(), this.servers[1].getPorta());
        this.servers[1].adicionarUsuario("bola", "bola");
        this.servers[0].removerArquivo("bola", "pasta/arquivo.exe");
    }
}
